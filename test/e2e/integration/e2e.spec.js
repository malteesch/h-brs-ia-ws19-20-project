let lsCache = {
    load: function () {
        localStorage.setItem('accessToken', this.accessToken);
        localStorage.setItem('theme', this.theme);
    }
};

describe('Start process as HR', function () {

    loginAsHR();

    it('open unstarted process', function () {
        cy.get('#hr-unstarted > div.ng-star-inserted > :nth-child(1)').click();
    });

    it('click start process', function () {
        cy.get('.dialog-actions > .mat-flat-button').click();
    });

    it('enter social performance values', function () {
        cy.get('#mat-input-2').click().type('3');
        cy.get('#mat-input-3').click().type('3');
        cy.get('#mat-input-4').click().type('5');
        cy.get('#mat-input-5').click().type('3');
        cy.get('#mat-input-6').click().type('4');
        cy.get('#mat-input-7').click().type('4');
    });

    it('start process', function () {
        lsCache.load();
        cy.get('#submit-button').click();
        cy.contains('Process started');
    });
});

describe('Approve process as CEO', function () {

    loginAsCEO();

    it('open unapproved process', function () {
        cy.get('#ceo-unapproved > div.ng-star-inserted > :nth-child(1)').click();
    });

    it('click approve and feedback', function () {
        cy.get('.dialog-actions > .mat-flat-button').click();
    });

    it('enter feedback and bonus + approve', function () {
        cy.get('#mat-input-2').click().type('{selectall}').type('50');
        cy.get('.mat-checkbox-inner-container').click();
        cy.get('#area').type('{selectall}').type('This is a feedback');
    });

    it('approve process', function () {
        lsCache.load();
        cy.get('.buttons > .mat-flat-button').click();
        cy.contains('Process approved');
    });
});

describe('Approve process as HR', function () {

    loginAsHR();

    it('open unapproved process', function () {
        cy.get('.unapproved-process').click();
    });

    it('click approve', function () {
        cy.get('.dialog-actions > .mat-flat-button').click();
    });

    it('approve', function () {
        cy.get('.mat-checkbox-inner-container').click();
    });

    it('approve process', function () {
        lsCache.load();
        cy.get('.buttons > .mat-flat-button').click();
        cy.contains('Process approved');
    });
});

describe('Approve process as Salesman', function () {

    loginAsSalesman();

    it('open accept dialog', function () {
        cy.get('.panel-description > .ng-star-inserted').click();
    });

    it('accept process', function () {
        lsCache.load();
        cy.get('.button-container > :nth-child(2)').click();
        cy.contains('Process accepted and finished');
    });
});

describe('Update bonus in OrangeHRM', function () {
    it('should have a value other than 0 in OrangeHRM', function () {
        cy.visit('https://sepp-hrm.inf.h-brs.de/symfony/web/index.php/auth/login');
        cy.get('#txtUsername').click().type('tom123');
        cy.get('#txtPassword').click().type('*Safb02da42Adm$');
        cy.get('#btnLogin').click();
        cy.get('#menu_pim_viewPimModule > b').click();
        cy.get(':nth-child(9) > :nth-child(3) > a').click();
        cy.get('#sidenav > :nth-child(7) > a').click();
        cy.get('#custom10').invoke('val').then(element => expect(element).to.be.greaterThan(0));
    });
});

function loginAsHR() {
    return it('Login as HR', function () {
        cy.visit('http://localhost:3000');
        cy.get('input#username').click().type('hr');
        cy.get('input#password').click().type('hr');
        cy.get('button.loginButton').click();
        cy.url().should('include', '/home').then(() => {
            lsCache.accessToken = localStorage.getItem('accessToken');
            lsCache.theme = localStorage.getItem('theme');
        });
    });
}

function loginAsCEO() {
    return it('Login as CEO', function () {
        cy.visit('http://localhost:3000');
        cy.get('input#username').click().type('ceo');
        cy.get('input#password').click().type('ceo');
        cy.get('button.loginButton').click();
        cy.url().should('include', '/home').then(() => {
            lsCache.accessToken = localStorage.getItem('accessToken');
            lsCache.theme = localStorage.getItem('theme');
        });
    });
}

function loginAsSalesman() {
    return it('Login as Salesman', function () {
        cy.visit('http://localhost:3000');
        cy.get('input#username').click().type('salesman');
        cy.get('input#password').click().type('salesman');
        cy.get('button.loginButton').click();
        cy.url().should('include', '/profile').then(() => {
            lsCache.accessToken = localStorage.getItem('accessToken');
            lsCache.theme = localStorage.getItem('theme');
        });
        cy.contains('You are only allowed to view your own profile');
    });
}
