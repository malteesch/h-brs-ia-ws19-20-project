const fs = require('fs');
const path = require('path');
const {describe, it} = require('mocha');
const sinon = require('sinon');
const request = require('supertest');
const axios = require('axios');

require('dotenv').config();


describe('Integration tests', () => {
    let Server;
    let accessToken;
    const OpenCRXService = require('../../src/server/service/OpenCRXService');
    const openCRXStub = sinon.stub(OpenCRXService);

    const OrangeHRMService = require('../../src/server/service/OrangeHRMService');
    const orangeHRMStub = sinon.stub(OrangeHRMService);

    before((done) => {
        const allProductsData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/all-products.json')).toString());
        const allCustomersData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/all-customers.json')).toString());
        const allOrdersData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/all-orders.json')).toString());
        const allPositionsData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/all-positions.json')).toString());
        const allContactsData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/all-contacts.json')).toString());
        openCRXStub.connect.resolves();
        openCRXStub.getAllProducts.resolves(allProductsData);
        openCRXStub.getAllCustomers.resolves(allCustomersData);
        openCRXStub.getAllOrders.resolves(allOrdersData);
        openCRXStub.getPositions.resolves(allPositionsData);
        openCRXStub.getAllContacts.resolves(allContactsData);

        const employeeSevenData = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/7.json')).toString());
        orangeHRMStub.connect.resolves();
        orangeHRMStub.searchEmployee.resolves(employeeSevenData);

        Server = require('../../src/server/server').Server;

        axios.post('http://localhost:3000/api/auth/login', {
            username: 'ceo',
            password: 'ceo'
        }, {
            headers: {
                'Accept': 'application/json'
            }
        }).then(response => {
            accessToken = response.data.accessToken;
            done();
        }).catch(err => {
            console.log(err);
            done();
        });
    });

    after(() => {
        openCRXStub.connect.reset();
        openCRXStub.getAllProducts.reset();
        openCRXStub.getAllCustomers.reset();
        openCRXStub.getAllOrders.reset();
        openCRXStub.getPositions.reset();
        openCRXStub.getAllContacts.reset();

        orangeHRMStub.connect.reset();
        orangeHRMStub.searchEmployee.reset();
    });
    describe('OpenCRX integration test', () => {
        it('should refresh the local copies of OpenCRX data in the database', (done) => {
            request(Server)
                .patch('/api/legacy/orders/refresh')
                .set('Authorization', `Bearer ${accessToken}`)
                .expect(204)
                .end(done);
        });
    });
    describe('OrangeHRM integration test', () => {
        it('should refresh the local copies of OpenCRX data in the database', (done) => {
            request(Server)
                .patch('/api/salesmen/refresh')
                .set('Authorization', `Bearer ${accessToken}`)
                .expect(204)
                .end(done);
        });
    });
});
