# Disclaimer 
!!! NOT PRODUCTION READY !!!
Do not expose on a publicly accessible server.

# Prerequisites
1. You need a running instance of MondoDB (locally installed, container, MongoDB Atlas,...)
2. Create a ``.env`` file in the root of the project and provide the following keys:
```dotenv
DB_USER= #MongoDB User
DB_PASSWORD= #MongoDB Password 
DB_HOST= #MongoDB hostname 
DB_PORT= #MongoDB Port 
DB_DATABASE= #MongoDB Database name 
DB_AUTHSOURCE= #MongoDB Database name for authentication 
NODE_ENV=prod #prod = no Traces in API Errors, dev = Traces in API Errors 
JWT_SECRET= #a secret to sign tokens with 
BCRYPT_SALT_ROUNDS=12 #Number of salt rounds for bcrypt, at least 12 recommended 
ORANGEHRM_CLIENT_ID= #OrangeHRM generated oauth client id 
ORANGEHRM_CLIENT_SECRET= #OrangeHRM generated oauth client secret 
OPENCRX_USER= #OpenCRX username 
OPENCRX_PASSWORD= #OpenCRX password 
```
# Install
1. Clone the repository
2. Open a terminal window in the project root and run ``npm install``
3. Run ``npm run build:production``
4. Run ``npm run start:production``
5. Open [localhost:3000](http://localhost:3000)

# Development
1. Open a terminal window in the project root and run ``npm run start:dev:server``
2. Open another terminal window and run ``npm run start:dev:ui``

API is now accessible on [localhost:3000/api](http://localhost:3000/api). The UI Server runs on [localhost:4200](http://localhost:4200).

## Registering Users
To register users for the process send a POST request to [localhost:3000/api/auth/register](http://localhost:3000/api/auth/register) and provide the following info in the body.
```json
{
	"username": "a username",
	"password": "a password",
	"employeeId": 1,
	"roles": ["ADMIN"],
	"fullName": "Full Name",
	"jobTitle": "Full Job Title"
}
```
Roles can be the following values and will lead to the corresponding privileges:
- ADMIN -> not relevant for process
- CEO
- HR
- SALESMAN

If you register a salesman make sure the employeeId matches _governmentId_ from OpenCRX and _code_ from OrangeHRM

