const Salesman = require('../model/SalesmanSchema').Salesman;
const PerformanceRecord = require('../model/PerformanceRecordSchema').PerformanceRecord;
const SettingsService = require('../service/SettingsService');
const OrangeHRMService = require('../service/OrangeHRMService');
const {NotFound, Forbidden} = require('http-errors');

async function getAllPerformanceRecords(employeeId) {
    const salesman = await _getSalesmanById(employeeId);
    return salesman.performanceRecords;
}

async function getOnePerformanceRecord(recordId, employeeId) {
    const salesman = await _getSalesmanById(employeeId);
    const record = await PerformanceRecord.findOne({id: recordId, employee: salesman._id});
    if (!record) throw new NotFound();
    return record;
}

async function addPerformanceRecord(newRecord, employeeId) {
    const salesman = await _getSalesmanById(employeeId);
    newRecord.employee = salesman;
    const record = new PerformanceRecord(newRecord);
    await record.save();
    salesman.performanceRecords.push(record);
    await salesman.save();
    return record;
}

async function updatePerformanceRecord(recordId, employeeId, changes) {
    const salesman = await _getSalesmanById(employeeId);
    return PerformanceRecord.updateOne({id: recordId, employee: salesman._id}, changes);
}

async function deletePerformanceRecord(recordId, employeeId) {
    const salesman = await _getSalesmanById(employeeId);
    salesman.performanceRecords = salesman.performanceRecords.filter(record => {
        return record.id !== +recordId;
    });
    await salesman.save();
    return PerformanceRecord.deleteOne({id: recordId});
}

async function generateRecord(year, socialPerformance, employeeId) {
    let settings = await SettingsService.getSettings(year);
    if (!settings) {
        // default settings in case db does not have a settings record
        settings = SettingsService.defaultSettings;
    }
    const socialScores = await Promise.all(socialPerformance.map(async score => {
        const targetValueSetting = settings.socialPerformanceTargetValues.find(element => element.id === score.id);
        return {
            category: targetValueSetting.category,
            targetValue: targetValueSetting.targetValue,
            actualValue: score.score,
            id: score.id,
            bonus: calculateSocialBonus(targetValueSetting.targetValue, score.score, settings.socialPerformanceCalculationParams),
            approved: false
        }
    }));
    const salesman = await Salesman.findOne({employeeId}).populate({path: 'orders', populate: 'customer'});
    const salesScores = (await Promise.all(salesman.orders
        .filter(order => new Date(order.date).getFullYear() === year)
        .map(order => order.positions
            .map(position => {
                return {
                    product: position.product,
                    customer: order.customer,
                    itemsSold: position.amount,
                    bonus: calculateSalesBonus(position.amount, order.customer, settings.customerRatingFactors),
                    approved: false
                };
            })))).flat();
    const completeCalculatedBonus =
        socialScores.reduce((acc, score) => acc + score.bonus, 0) +
        salesScores.reduce((acc, score) => acc + score.bonus, 0);
    return {
        id: employeeId.toString() + year,
        processStatus: {
            finished: false,
            approvedByCEO: false,
            approvedByHR: false,
            approvedByEmployee: false,
            feedbackGiven: false,
            started: true,
        },
        socialScores,
        salesScores,
        year,
        bonus: {
            calculatedBonus: completeCalculatedBonus,
            extraBonus: 0
        }
    };
}

async function startCalculationprocess(year, socialPerformance, employeeId) {
    const newPerformanceCalculation = await generateRecord(year, socialPerformance, employeeId);
    return await addPerformanceRecord(newPerformanceCalculation, employeeId);
}

async function updateCalculationProcess(year, socialPerformance, employeeId) {
    const record = await generateRecord(year, socialPerformance, employeeId);
    const update = {
        id,
        socialScores,
        salesScores,
        bonus,
        year,
    } = record;
    return await updatePerformanceRecord(update.id, employeeId, update);
}

function calculateSocialBonus(targetValue, actualValue, settings) {
    const diff = actualValue - targetValue;
    if (diff <= settings.maxValNoBonus) {
        return 0;
    }
    if (diff > settings.maxValNoBonus && diff < 0) {
        return settings.fixValAlmostReached;
    }
    return settings.fixValReached + (diff * settings.factorReached);
}

function calculateSalesBonus(itemsSold, customer, factors) {
    function determineFactor() {
        switch (customer.rating) {
            case 1:
                return factors.firstLevelFactor;
            case 2:
                return factors.secondLevelFactor;
            case 3:
                return factors.thirdLevelFactor;
        }
    }

    return itemsSold * 10 * determineFactor();
}

async function _getSalesmanById(id) {
    const salesman = await Salesman.findOne({employeeId: id}).populate('performanceRecords');
    if (!salesman) {
        throw new NotFound('Can\'t find employee');
    }
    return salesman;
}

async function approveBySalesman(processId, employeeId) {
    const record = await PerformanceRecord.findOne({id: processId});
    if (record === null) {
        throw new NotFound('No such record');
    }
    if (!record.processStatus.approvedByCEO || !record.processStatus.approvedByHR) {
        throw new Forbidden('You cannot approve this process yet');
    }
    record.processStatus = {
        ...record.processStatus,
        approvedByEmployee: true,
        finished: true
    };
    await record.save();
    const salesman = await _getSalesmanById(employeeId);
    return await OrangeHRMService.updateBonusPayment(salesman.hrmId, record.bonus.calculatedBonus + record.bonus.extraBonus);
}

module.exports = {
    getOnePerformanceRecord,
    getAllPerformanceRecords,
    addPerformanceRecord,
    updatePerformanceRecord,
    deletePerformanceRecord,
    startCalculationprocess,
    updateCalculationProcess,
    approveBySalesman
};
