const OpenCRXService = require('./OpenCRXService');
const Customer = require('../model/CustomerSchema').Customer;

async function refreshCustomers() {
    const customers = await OpenCRXService.getAllCustomers();
    return Customer.bulkWrite(
        customers.map(customer => ({
            updateOne: {
                filter: {
                    id: (() => {
                        const hrefString = customer['@href'].split('/');
                        return hrefString[hrefString.length - 1];
                    })(),
                },
                update: {
                    $set: {
                        name: customer.fullName,
                        rating: customer['accountRating'],
                    }
                },
                upsert: true,
            }
        }))
    );
}

module.exports = {
    refreshCustomers,
};
