const axios = require('axios');

const baseUrl = 'https://sepp-crm.inf.h-brs.de/opencrx-rest-CRX';
const credentials = {
    username: process.env.OPENCRX_USER,
    password: process.env.OPENCRX_PASSWORD,
};

async function connect() {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };
    //simple request to check if the service is online
    await axios.get(`${baseUrl}/org.opencrx.kernel.home1/provider/CRX/segment/Standard/userHome/guest/accessHistory`, config);
}

async function getAllProducts() {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };

    const products = await axios.get(`${baseUrl}/org.opencrx.kernel.product1/provider/CRX/segment/Standard/product`, config);
    return products.data.objects;
}

async function getAllCustomers() {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };
    const contacts = await axios.get(`${baseUrl}/org.opencrx.kernel.account1/provider/CRX/segment/Standard/account`, config);
    return contacts.data.objects.filter(contact => contact['@type'] === 'org.opencrx.kernel.account1.LegalEntity');
}

async function getAllContacts() {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };
    const contacts = await axios.get(`${baseUrl}/org.opencrx.kernel.account1/provider/CRX/segment/Standard/account`, config);
    return contacts.data.objects;
}

async function getAllOrders() {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };
    const orders = await axios.get(`${baseUrl}/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder`, config);
    return orders.data.objects;
}

async function getPositions(orderId) {
    const config = {
        headers: {
            'Accept': 'application/json'
        },
        auth: credentials,
    };
    const positions = await axios.get(`${baseUrl}/org.opencrx.kernel.contract1/provider/CRX/segment/Standard/salesOrder/${orderId}/position`, config);
    return positions.data.objects;
}

module.exports = {
    connect,
    getAllProducts,
    getAllCustomers,
    getAllOrders,
    getAllContacts,
    getPositions,
};
