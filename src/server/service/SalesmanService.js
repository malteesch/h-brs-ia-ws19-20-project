const Salesman = require('../model/SalesmanSchema').Salesman;
const Order = require('../model/OrderSchema').Order;
const OrangeHRMService = require('./OrangeHRMService');

async function getAll() {
    return Salesman.find()
        .populate('performanceRecords')
        .populate({path: 'orders', populate: 'customer'});
}

async function getOne(id) {
    return Salesman.findOne({employeeId: id})
        .populate('performanceRecords')
        .populate({path: 'orders', populate: 'customer'});
}

async function createOne(salesman) {
    return new Salesman(salesman).save();
}

async function updateOne(id, salesman) {
    return Salesman.updateOne({employeeId: id}, salesman);
}

async function deleteOne(id) {
    return Salesman.deleteOne({employeeId: id});
}

async function refreshRecords() {
    let salesmen = await OrangeHRMService.searchEmployee({unit: 2});
    return Salesman.bulkWrite(
        salesmen.map(salesman => ({
            updateOne: {
                filter: {employeeId: parseInt(salesman.code)},
                update: {
                    $set: {
                        name: salesman.fullName,
                        employeeId: parseInt(salesman.code),
                        jobTitle: salesman.jobTitle,
                        imgUrl: `${salesman.fullName.replace(/\s/g, '').toLowerCase()}.png`,
                        hrmId: parseInt(salesman.employeeId, 10)
                    }
                },
                upsert: true,
            }
        }))
    );
}

async function refreshOrders() {
    const salesmen = await Salesman.find();
    salesmen.forEach(await (async salesman => {
        const orders = await Order.find({employee: salesman});
        orders.forEach(order => {
            const index = salesman.orders.indexOf(order['_id']);
            if (index !== -1) {
                salesman.orders[index] = order;
            } else {
                salesman.orders.push(order);
            }
        });
        salesman.save();
    }));
}

module.exports = {
    getAll,
    getOne,
    createOne,
    updateOne,
    deleteOne,
    refreshRecords,
    refreshOrders,
};
