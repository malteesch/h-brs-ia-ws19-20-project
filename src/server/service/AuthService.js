const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../model/UserSchema').User;
const {Unauthorized, Conflict, Forbidden} = require('http-errors');

async function register(user) {
    const existingUser = await User.findOne({username: user.username});
    if (existingUser) {
        throw new Conflict('User already exists');
    }
    const plaintext = user.password;
    user.password = await bcrypt.hash(plaintext, +process.env.BCRYPT_SALT_ROUNDS || 12);
    const registeredUser = new User(user);
    await registeredUser.save();
    return await login({
        username: user.username,
        password: plaintext
    })
}

async function login(credentials) {
    const user = await User.findOne({username: credentials.username});
    if (!user) {
        throw new Unauthorized('Invalid credentials');
    }
    const match = await bcrypt.compare(credentials.password, user.password);

    if (match) {
        return await generateToken(user);
    }
    throw new Unauthorized('Invalid credentials');
}

async function generateToken(user) {
    const token = await jwt.sign({
            username: user.username,
            roles: user.roles,
            id: user.employeeId,
            fullName: user.fullName,
            jobTitle: user.jobTitle
        },
        process.env.JWT_SECRET,
        {
            expiresIn: '24h'
        });
    return {
        accessToken: token,
        tokenType: 'JWT',
        expiresIn: 86400
    }
}

function tokenAuthenticator(req, res, next) {
    let token = req.headers.authorization;
    if (!token) {
        throw new Unauthorized();
    }
    if (!token.startsWith('Bearer ')) {
        throw new Unauthorized('Invalid token shape');
    }
    token = token.slice(7, token.length);
    jwt.verify(token, process.env.JWT_SECRET, (err, payload) => {
        if (err === null) {
            req.payload = payload;
            next()
        } else {
            next(new Unauthorized('Invalid token'));
        }
    })
}

const Roles = {
    ADMIN: 'ADMIN',
    CEO: 'CEO',
    HR: 'HR',
    SALESMAN: 'SALESMAN'
};

function roleAuthenticator(...roles) {
    return function (req, res, next) {
        if (roles.filter(role => req.payload.roles.find(element => element === role)).length > 0) {
            next();
        } else {
            throw new Forbidden('You don\'t have access to the requested route');
        }
    }
}

module.exports = {
    register,
    login,
    tokenAuthenticator,
    roleAuthenticator,
    Roles
};
