const OpenCRXService = require('./OpenCRXService');
const Product = require('../model/ProductSchema').Product;

async function refreshProducts() {
    const products = await OpenCRXService.getAllProducts();
    return Product.bulkWrite(
        products.map(product => ({
            updateOne: {
                filter: {
                    id: (() => {
                        const hrefString = product['@href'].split('/');
                        return hrefString[hrefString.length - 1];
                    })(),
                },
                update: {
                    $set: {
                        name: product.name,
                    }
                },
                upsert: true,
            }
        }))
    );
}

module.exports = {
    refreshProducts,
};
