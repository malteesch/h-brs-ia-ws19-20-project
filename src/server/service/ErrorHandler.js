const {Conflict, BadRequest, InternalServerError} = require('http-errors');

const errorHandler = function (err, req, res, next) {

    let error = {
        path: req.path,
        method: req.method,
        timestamp: new Date().getTime(),
    };
    if (process.env.NODE_ENV === 'dev') {
        error.trace = err.stack;
    }
    switch (err.name) {
        case 'MongoError':
            err = checkMongoError(err);
            break;
        case 'ValidationError':
            err = new BadRequest(err._message);
            break;
        case 'Error':
            err = new InternalServerError(err._message);
            break;
    }
    error.message = err.message;

    res.status(err.status).json(error);
};

function checkMongoError(err) {
    switch (err.code) {
        case 11000:
            return new Conflict('Duplicate on unique field');
        default:
            return new InternalServerError();
    }
}

module.exports = errorHandler;
