const Settings = require('../model/SettingsSchema').Settings;
const PerformanceRecord = require('../model/PerformanceRecordSchema').PerformanceRecord;

const defaultSettings = {
    yearOfValidity: new Date().getFullYear(),
    socialPerformanceTargetValues: [
        {
            category: "Leadership Competence",
            targetValue: 4,
            actualValue: 3,
            id: 1
        },
        {
            category: "Openness to Employee",
            targetValue: 4,
            actualValue: 4,
            id: 2
        },
        {
            category: "Social Behaviour to Employee",
            targetValue: 4,
            actualValue: 7,
            id: 3
        },
        {
            category: "Attitude toward Client",
            targetValue: 4,
            actualValue: 4,
            id: 4
        },
        {
            category: "Communication Skills",
            targetValue: 4,
            actualValue: 7,
            id: 5
        },
        {
            category: "Integrity to Company",
            targetValue: 4,
            actualValue: 7,
            id: 6
        }
    ],
    socialPerformanceCalculationParams: {
        maxValNoBonus: -2,
        fixValAlmostReached: 20,
        fixValReached: 50,
        factorReached: 50
    },
    customerRatingFactors: {
        firstLevelFactor: 5,
        secondLevelFactor: 3,
        thirdLevelFactor: 1,
    },
    default: true
};

async function addSettings(settings) {
    let newSettings = new Settings(settings);
    newSettings = await newSettings.save();
    return newSettings;
}

async function getAllSettings() {
    const allSettings = await Settings.find({}).sort({yearOfValidity: 'desc'});
    if (allSettings.length === 0) {
        return [defaultSettings];
    }
    return allSettings;
}

async function getSettings(year) {
    const settings = await Settings.findOne({yearOfValidity: year});
    if (settings === null) {
        const allSettings = await getAllSettings();
        return allSettings[0];
    }
    return settings;
}

async function updateSettings(year, payload) {
    const {updateCalculationProcess} = require('../service/PerformanceRecordService');
    const update = await Settings.findOneAndUpdate({yearOfValidity: year}, payload, {upsert: true});
    const openRecords = await PerformanceRecord.find({['processStatus.approvedByCEO']: false}).populate('employee');
    if (openRecords.length > 0) {
        await Promise.all(openRecords.map(record => updateCalculationProcess(record.year, record.socialScores.map(score => ({
            id: score.id,
            score: score.actualValue
        })), record.employee.employeeId)));
    }
    return update;
}

async function deleteSettings(year) {
    return Settings.deleteOne({yearOfValidity: year});
}

module.exports = {
    defaultSettings,
    addSettings,
    getAllSettings,
    getSettings,
    updateSettings,
    deleteSettings
};
