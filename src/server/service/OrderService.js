const OpenCRXService = require('./OpenCRXService');
const Order = require('../model/OrderSchema').Order;
const Salesman = require('../model/SalesmanSchema').Salesman;
const Customer = require('../model/CustomerSchema').Customer;
const Product = require('../model/ProductSchema').Product;

async function refreshAllOrders() {
    const orders = await OpenCRXService.getAllOrders();
    const contacts = await OpenCRXService.getAllContacts();
    const writes = await Promise.all(orders.map(async order => {
        const hrefArrayOrderSalesRep = order['salesRep']['@href'].split('/');
        const salesman = contacts.find(contact => {
            let hrefArrayContact = contact['@href'].split('/');
            return hrefArrayOrderSalesRep[hrefArrayOrderSalesRep.length - 1] === hrefArrayContact[hrefArrayContact.length - 1];
        });

        const employee = await Salesman.findOne({employeeId: salesman['governmentId']});

        const hrefArrayCustomer = order['customer']['@href'].split('/');
        const customer = await Customer.findOne({id: hrefArrayCustomer[hrefArrayCustomer.length - 1]});
        const hrefArrayOrder = order['@href'].split('/');
        let positions = await OpenCRXService.getPositions(hrefArrayOrder[hrefArrayOrder.length - 1]);
        positions = await Promise.all(positions.map(async position => {
            const hrefStringProduct = position['product']['@href'].split('/');
            const productId = hrefStringProduct[hrefStringProduct.length - 1];
            const product = await Product.findOne({id: productId});
            return {
                product: {
                    name: product.name,
                    id: productId
                },
                amount: parseInt(position['quantityBackOrdered'])
            }
        }));
        return {
            updateOne: {
                filter: {
                    id: (() => {
                        const hrefString = order['@href'].split('/');
                        return hrefString[hrefString.length - 1];
                    })(),
                },
                update: {
                    $set: {
                        employee,
                        customer,
                        total: parseFloat(order['totalBaseAmount']),
                        positions,
                        date: new Date(order['activeOn'])
                    }
                },
                upsert: true,
            }
        }
    }));
    return Order.bulkWrite(
        writes
    );
}

async function getAllOrders() {
    return Order.find().populate({
        path: 'employee',
        select: ['name', 'employeeId']
    }).populate('customer').populate('performanceRecords');
}

module.exports = {
    refreshAllOrders,
    getAllOrders,
};
