const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const performanceRecordSchema = new Schema({
    id: {type: Number, required: true, unique: true},
    employee: {type: Schema.Types.ObjectId, ref: 'Salesman', required: true},
    socialScores: [{
        id: {type: Number, required: true},
        category: {type: String, required: true},
        targetValue: {type: Number, required: true},
        actualValue: {type: Number, required: true},
        bonus: {type: Number, required: true},
        approved: {type: Boolean, default: false, required: true}
    }],
    salesScores: [{
        product: {
            id: {type: String, required: true},
            name: {type: String, required: true}
        },
        customer: {
            id: {type: String, required: true},
            name: {type: String, required: true},
            rating: {type: Number, required: true}
        },
        itemsSold: {type: Number, required: true},
        bonus: {type: Number, required: true},
        approved: {type: Boolean, default: false, required: true}
    }],
    year: {type: Number, required: true},
    processStatus: {
        type: {
            finished: {type: Boolean, default: false},
            feedbackGiven: {type: Boolean, default: false},
            started: {type: Boolean, default: false},
            approvedByCEO: {type: Boolean, default: false},
            approvedByHR: {type: Boolean, default: false},
            approvedByEmployee: {type: Boolean, default: false},
        }
    },
    bonus: {
        type: {
            calculatedBonus: {type: Number, default: 0},
            extraBonus: {type: Number, default: 0}
        }
    },
    feedback: {
        type: {
            text: {type: String, required: true},
            fromUser: {
                type: {
                    fullName: {type: String, required: true},
                    jobTitle: {type: String, required: true}
                },
                required: true
            }
        },
        required: false
    }
}, {
    versionKey: false
});

const PerformanceRecord = mongoose.model('PerformanceRecord', performanceRecordSchema);

module.exports = {PerformanceRecord, performanceRecordSchema};
