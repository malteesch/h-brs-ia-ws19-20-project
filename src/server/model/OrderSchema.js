const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    id: {type: String, required: true, index: true, unique: true},
    employee: {type: Schema.Types.ObjectId, ref: 'Salesman'},
    customer: {type: Schema.Types.ObjectId, ref: 'Customer'},
    date: {type: Date, required: true},
    positions: {
        type: [{
            product: {
                name: {type: String, required: true},
                id: {type: String, required: true}
            },
            amount: {type: Number, required: true}
        }], required: false
    },
    total: {type: Number, required: true}
}, {
    versionKey: false
});

const Order = mongoose.model('Order', orderSchema);

module.exports = {Order, orderSchema};
