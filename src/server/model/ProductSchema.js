const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    id: {type: String, required: true, index: true, unique: true},
    name: {type: String, required: true},
}, {
    versionKey: false
});

const Product = mongoose.model('Product', productSchema);

module.exports = {Product, productSchema};
