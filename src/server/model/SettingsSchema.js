const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const settingsSchema = new Schema({
    yearOfValidity: {type: Number, unique: true, required: true},
    socialPerformanceTargetValues: {
        type: [{
            id: {type: Number, required: true},
            category: {type: String, required: true},
            targetValue: {type: Number, required: true}
        }]
    },
    socialPerformanceCalculationParams: {
        type: {
            maxValNoBonus: {type: Number, required: true},
            fixValAlmostReached: {type: Number, required: true},
            fixValReached: {type: Number, required: true},
            factorReached: {type: Number, required: true}
        }
    },
    customerRatingFactors: {
        firstLevelFactor: {type: Number, required: true},
        secondLevelFactor: {type: Number, required: true},
        thirdLevelFactor: {type: Number, required: true},
    },
    default: {type: Boolean, default: false}
});

const Settings = mongoose.model('Settings', settingsSchema);

module.exports = {
    Settings,
    settingsSchema
};
