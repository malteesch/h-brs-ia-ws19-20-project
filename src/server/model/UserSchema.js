const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    roles: {type: [String], required: true},
    employeeId: {type: Number, required: true, unique: true},
    fullName: {type: String, required: true},
    jobTitle: {type: String, required: false}
});

const User = mongoose.model('User', userSchema);

module.exports = {
    User,
    userSchema
};
