const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const salesmanSchema = new Schema({
    name: {type: String, required: true},
    employeeId: {type: Number, unique: true, index: true, required: true},
    performanceRecords: [{type: Schema.Types.ObjectId, ref: 'PerformanceRecord'}],
    opencrxident: String,
    orders: [{type: Schema.Types.ObjectId, ref: 'Order', required: false}],
    jobTitle: {type: String, required: true},
    imgUrl: {type: String},
    hrmId: {type: Number}
}, {
    versionKey: false
});

const Salesman = mongoose.model('Salesman', salesmanSchema);

module.exports = {Salesman, salesmanSchema};
