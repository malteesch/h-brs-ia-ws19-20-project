const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    id: {type: String, required: true, index: true, unique: true},
    name: {type: String, required: true},
    rating: {type: Number, required: true}
}, {
    versionKey: false
});

const Customer = mongoose.model('Customer', customerSchema);

module.exports = {Customer, customerSchema};
