const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('./service/ErrorHandler');
const OrangeHrmService = require('./service/OrangeHRMService');
const OpenCRXService = require('./service/OpenCRXService');

const SalesmanService = require('./service/SalesmanService');
const ProductService = require('./service/ProductService');
const CustomerService = require('./service/CustomerService');
const OrderService = require('./service/OrderService');

/*
 * Database connection
 */
const dbUser = process.env.DB_USER;
const dbPassword = process.env.DB_PASSWORD;
const dbHost = process.env.DB_HOST;
const dbPort = process.env.DB_PORT;
const dbDatabase = process.env.DB_DATABASE;
const dbAuthSource = process.env.DB_AUTHSOURCE;

const db = mongoose.connect(
    `mongodb://${dbUser}:${dbPassword}@${dbHost}:${dbPort}/${dbDatabase}?authSource=${dbAuthSource}`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    })
    .then(() => console.info('\x1b[36m%s\x1b[0m', 'Connected to MongoDB'))
    .catch(error => console.error(error));

const hrm = OrangeHrmService.connect()
    .then(() => console.log('\x1b[36m%s\x1b[0m', 'Connected to OrangeHRM'))
    .catch(err => console.error('\x1b[31m%s\x1b[0m', `OrangeHRM: ${err.message ? err.message : err.code}`));

const crx = OpenCRXService.connect()
    .then(() => console.log('\x1b[36m%s\x1b[0m', 'Connected to OpenCRX'))
    .catch(err => console.error('\x1b[31m%s\x1b[0m', `OpenCRX: ${err.message}`));

Promise.all([db, hrm, crx])
    .then(async () => {
        await SalesmanService.refreshRecords();
        await ProductService.refreshProducts();
        await CustomerService.refreshCustomers();
        await OrderService.refreshAllOrders();
        await SalesmanService.refreshOrders();
    })
    .then(() => console.log('\x1b[32m%s\x1b[0m', 'Records from legacy systems updated'))
    .catch(err => console.error('\x1b[31m%s\x1b[0m', `Error: ${err.message ? err.message : err.code}`));

const ApiController = require('./controller/ApiController');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, '../../public/gui')));
app.use(express.static(path.join(__dirname, '../../public/img')));

app.use('/api', ApiController);
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../../public/gui/index.html'));
});
app.use(errorHandler);

module.exports = {
    Server: app
};
