const express = require('express');
const router = express.Router();
const CustomerService = require('../service/CustomerService');

router.patch('/refresh', async function (req, res, next) {
    CustomerService.refreshCustomers()
        .then(() => res.status(204).send())
        .catch(error => next(error));
});

module.exports = router;
