const express = require('express');
const router = express.Router();
const SettingsService = require('../service/SettingsService');

router.post('/', async function (req, res, next) {
    SettingsService.addSettings(req.body)
        .then(result => res.send())
        .catch(err => next(err));
});

router.get('/', async function (req, res, next) {
    SettingsService.getAllSettings()
        .then(result => res.json(result))
        .catch(err => next(err));
});

router.get('/:year', async function (req, res, next) {
    SettingsService.getSettings(req.params.year)
        .then(result => res.json(result))
        .catch(err => next(err));
});

router.put('/:year', async function (req, res, next) {
    SettingsService.updateSettings(req.params.year, req.body)
        .then(() => res.send())
        .catch(err => next(err));
});

router.delete('/:year', function (req, res, next) {
    SettingsService.deleteSettings(req.params.year)
        .then(() => res.send())
        .catch(err => next(err));
});

module.exports = router;
