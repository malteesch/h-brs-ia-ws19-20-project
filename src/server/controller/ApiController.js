const express = require('express');
const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger');

const AuthController = require('./AuthController');
const SalesmanController = require('./SalesmanController');
const AuthService = require('../service/AuthService');
const LegacySystemsController = require('./LegacySystemsController');
const SettingsController = require('../controller/SettingsController');
const Roles = AuthService.Roles;


router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));
router.use('/auth', AuthController);

router.use(AuthService.tokenAuthenticator);
/*
 * PROTECTED ROUTES (DO NOT MOVE ABOVE TOKENCHECKER)
 */
router.use('/salesmen', AuthService.roleAuthenticator(...Object.values(Roles)), SalesmanController);
router.use('/settings', AuthService.roleAuthenticator(Roles.ADMIN, Roles.CEO, Roles.HR), SettingsController);
router.use('/legacy', AuthService.roleAuthenticator(Roles.ADMIN, Roles.CEO), LegacySystemsController);

module.exports = router;
