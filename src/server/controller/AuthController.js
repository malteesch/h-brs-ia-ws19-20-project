const express = require('express');
const router = express.Router();

const AuthService = require('../service/AuthService');

router.post('/register', async function (req, res, next) {
    AuthService.register(req.body)
        .then(value => {
            res.status(201).json(value);
        })
        .catch(error => next(error));
});

router.post('/login', async function (req, res, next) {
    AuthService.login(req.body)
        .then(value => {
            res.json(value);
        })
        .catch(error => next(error));
});

module.exports = router;
