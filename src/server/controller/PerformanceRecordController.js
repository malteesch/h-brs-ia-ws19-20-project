const express = require('express');
const router = express.Router();
const AuthService = require('../service/AuthService');
const PerformanceRecordService = require('../service/PerformanceRecordService');
const {NotFound, Forbidden} = require('http-errors');

/*GET = GetAll
* sends all Records
*/
router.get('/', async function (req, res, next) {
    PerformanceRecordService.getAllPerformanceRecords(req.employeeId)
        .then(records => {
            res.json(records);
        })
        .catch(error => next(error));
});


/* GET = GetOne with ID
* sends one Records, identified by ID
*/
router.get('/:id', async function (req, res, next) {
    PerformanceRecordService.getOnePerformanceRecord(req.params.id, req.employeeId)
        .then(record => {
            res.json(record);
        })
        .catch(error => next(error));
});


/*POST = Create
* creates new Records
*/
router.post('/', async function (req, res, next) {
    PerformanceRecordService.addPerformanceRecord(req.body, req.employeeId)
        .then((record) => {
            res
                .set('Location', `/api/salesmen/${req.employeeId}/performancerecords/${record.id}`)
                .status(201)
                .send()
        })
        .catch(error => next(error));
});


/*PUT = Update
* updates Records, identified by ID
*/
router.put('/:id', async function (req, res, next) {
    PerformanceRecordService.updatePerformanceRecord(req.params.id, req.employeeId, req.body)
        .then((update) => {
            if (update.n < 1) {
                next(new NotFound());
            } else if (update.nModified < 1) {
                res.status(202).send()
            } else res.send();
        })
        .catch(error => next(error));
});


/*DELETE = Delete
* deletes Records, identified by ID
*/
router.delete('/:id', async function (req, res, next) {
    PerformanceRecordService.deletePerformanceRecord(req.params.id, req.employeeId)
        .then(removed => {
            if (removed.n < 1) {
                next(new NotFound());
            } else {
                res.send();
            }
        })
        .catch(error => next(error));
});

router.post('/start', AuthService.roleAuthenticator(AuthService.Roles.HR, AuthService.Roles.ADMIN), async function (req, res, next) {
    PerformanceRecordService.startCalculationprocess(req.body.year, req.body.socialPerformance, req['employeeId'])
        .then(() => res.send())
        .catch(err => next(err));
});

router.post('/:id/approve', AuthService.roleAuthenticator(AuthService.Roles.SALESMAN), async function (req, res, next) {
    if (parseInt(req.employeeId) !== req.payload.id) {
        next(new Forbidden('You are not allowed to access this route'));
    } else {
        PerformanceRecordService.approveBySalesman(req.params.id, req.employeeId)
            .then(() => res.send())
            .catch(err => next(err));
    }
});

module.exports = router;
