const express = require('express');
const router = express.Router();
const OrderController = require('./OrderController');
const CustomerController = require('./CustomerController');
const ProductController = require('./ProductController');

router.use('/orders', OrderController);
router.use('/customers', CustomerController);
router.use('/products', ProductController);

module.exports = router;
