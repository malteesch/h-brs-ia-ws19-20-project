const express = require('express');
const router = express.Router();
const SalesmanService = require('../service/SalesmanService');
const PerformanceRecordController = require('../controller/PerformanceRecordController');
const {NotFound, Forbidden} = require('http-errors');
const {roleAuthenticator, Roles} = require('../service/AuthService');

/*GET = GetAll
* sends all Salesmen
*/
router.get('/', roleAuthenticator(Roles.ADMIN, Roles.CEO, Roles.HR), async function (req, res) {
    res.json(await SalesmanService.getAll());
});


/* GET = GetOne with ID
* sends one Salesman, identified by ID
*/
router.get('/:id', function(req, res, next) {
    if (req.payload.roles.length === 1 && req.payload.roles[0] === Roles.SALESMAN) {
        if (+req.params.id === req.payload.id) {
            next()
        } else throw new Forbidden('You don\'t have access to the requested route');
    } else {
        next();
    }
}, async function (req, res, next) {
    SalesmanService.getOne(req.params.id)
        .then(entry => {
            if (entry === null) {
                throw new NotFound();
            }
            res.json(entry);
        })
        .catch(err => {
            next(err);
        })
});


/*POST = Create
* creates new Salesman
*/
router.post('/', async function (req, res, next) {
    SalesmanService.createOne(req.body)
        .then((entry) => {
            res
                .set('Location', `/api/salesmen/${entry._doc.employeeId}`)
                .status(201)
                .send();
        })
        .catch(error => next(error))
});


/*PUT = Update
* updates Salesman, identified by ID
*/
router.put('/:id', async function (req, res, next) {
    SalesmanService.updateOne(req.params.id, req.body)
        .then((update) => {
            if (update.n < 1) {
                next(new NotFound());
            } else if (update.nModified < 1) {
                res.status(202).send('Nothing was updated')
            } else res.send();
        })
        .catch(error => next(error));
});


/*DELETE = Delete
* deletes Salesman, identified by ID
*/
router.delete('/:id', async function (req, res, next) {
    SalesmanService.deleteOne(req.params.id)
        .then(removed => {
                if (removed.n < 1) {
                    next(new NotFound());
                } else {
                    res.send();
                }
            })
        .catch(error => next(error));
});

router.patch('/refresh', async function (req, res, next) {
    SalesmanService.refreshRecords()
        .then(() => {
            res.status(204).send();
        })
        .catch(error => next(error));
});

router.param('employeeId', function (req, res, next) {
    req.employeeId = req.params.employeeId;
    next();
});
router.use("/:employeeId/performancerecords", PerformanceRecordController);

module.exports = router;
