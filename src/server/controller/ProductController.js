const express = require('express');
const router = express.Router();
const ProductService = require('../service/ProductService');

router.patch('/refresh', async function (req, res, next) {
    ProductService.refreshProducts()
        .then(() => res.status(204).send())
        .catch(error => next(error));
});

module.exports = router;
