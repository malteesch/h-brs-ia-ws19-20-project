const express = require('express');
const router = express.Router();
const OrderService = require('../service/OrderService');

router.patch('/refresh', async function (req, res, next) {
    OrderService.refreshAllOrders()
        .then(() => res.status(204).send())
        .catch(error => next(error));
});

router.get('/', async function (req, res, next) {
    OrderService.getAllOrders()
        .then(orders => {
            res.json(orders);
        })
        .catch(error => next(error));
});

module.exports = router;
