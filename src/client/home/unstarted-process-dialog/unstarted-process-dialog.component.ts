import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Order, SalesManProfile, StartProcessPayload } from '../../app/model';
import { Observable, Subscription } from 'rxjs';
import { selectSalesmanById } from '../../app/store/app.selectors';
import { AppState } from '../../app/store/reducers';
import { Store } from '@ngrx/store';
import { StartProcessDialogComponent } from '../../performancerecords/start-process-dialog/start-process-dialog.component';

@Component({
    selector: 'app-start-process-dialog',
    templateUrl: './unstarted-process-dialog.component.html',
    styleUrls: ['./unstarted-process-dialog.component.scss']
})
export class UnstartedProcessDialogComponent implements OnInit, OnDestroy {

    salesman$: Observable<SalesManProfile>;
    startProcessDialogRef: MatDialogRef<StartProcessDialogComponent>;
    startProcessDialogRefClosed$: Subscription = new Subscription();

    constructor(
        @Inject(MAT_DIALOG_DATA) public process: Partial<StartProcessPayload>,
        private store: Store<AppState>,
        private dialog: MatDialog,
        private dialogRef: MatDialogRef<UnstartedProcessDialogComponent>
    ) {
    }

    ngOnInit() {
        this.salesman$ = this.store.select(selectSalesmanById(this.process.employeeId));
    }

    openStartProcess() {
        this.startProcessDialogRef = this.dialog.open(StartProcessDialogComponent, {
            data: this.process,
            autoFocus: false
        });
        this.startProcessDialogRefClosed$ = this.startProcessDialogRef.afterClosed().subscribe((saved: boolean) => {
            if (saved) {
                this.dialogRef.close();
            }
        });
    }

    ngOnDestroy() {
        this.startProcessDialogRefClosed$.unsubscribe();
    }

    filterOrders(orders: Array<Order>) {
        return orders.filter(order => this.process.year === new Date(order.date).getFullYear());
    }
}
