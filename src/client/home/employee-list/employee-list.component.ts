import { Component, OnInit } from '@angular/core';
import { AppState } from '../../app/store/reducers';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SalesManProfile } from '../../app/model';
import { selectSalesmen } from '../../app/store/app.selectors';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

    displayedColumns: string[] = ['employeeId', 'name', 'jobTitle', 'actions'];
    salesmen$: Observable<Array<SalesManProfile>>;

    constructor(private store: Store<AppState>) {
    }

    ngOnInit() {
        this.salesmen$ = this.store.select(selectSalesmen);
    }

}
