import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-approve-by-salesman-dialog',
    templateUrl: './approve-by-salesman-dialog.component.html',
    styleUrls: ['./approve-by-salesman-dialog.component.scss']
})
export class ApproveBySalesmanDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<ApproveBySalesmanDialogComponent>,
    ) {
    }
}
