import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { StartProcessDialogComponent } from '../../performancerecords/start-process-dialog/start-process-dialog.component';

@Component({
    selector: 'app-todo-dialog',
    templateUrl: './todo-dialog.component.html',
    styleUrls: ['./todo-dialog.component.scss']
})
export class TodoDialogComponent implements OnInit {

    constructor(
        @Inject(MAT_DIALOG_DATA) public process: { employeeId: number, year: number },
        private dialog: MatDialog
    ) {
    }

    ngOnInit() {
    }

    openStartDialog() {
        this.dialog.open(StartProcessDialogComponent, {data: this.process});
    }
}
