import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { selectUnstartedProcesses, selectSalesmanById } from '../../app/store/app.selectors';
import { Observable } from 'rxjs';
import { SalesManProfile } from '../../app/model';
import { MatDialog } from '@angular/material/dialog';
import { UnstartedProcessDialogComponent } from '../unstarted-process-dialog/unstarted-process-dialog.component';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-openworkflows',
    templateUrl: './openworkflows.component.html',
    styleUrls: ['./openworkflows.component.scss']
})
export class OpenworkflowsComponent implements OnInit {

    unstartedProcesses$ = this.store.select(selectUnstartedProcesses);
    hasUnstarted$ = this.unstartedProcesses$.pipe(
        map(processes => processes.length > 0),
    );

    constructor(private store: Store<AppState>, private dialog: MatDialog) {
    }

    ngOnInit() {
    }

    getSalesmanInfo(id: number): Observable<SalesManProfile> {
        return this.store.select(selectSalesmanById(id));
    }

    openTodos(unstartedProcess: { employeeId: number, year: number }) {
        this.dialog.open(UnstartedProcessDialogComponent, {
            data: unstartedProcess,
            autoFocus: false,
            width: '500px'
        });
    }
}
