import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { OpenworkflowsComponent } from './openworkflows/openworkflows.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { TodoDialogComponent } from './todo-dialog/todo-dialog.component';
import { UnstartedProcessDialogComponent } from './unstarted-process-dialog/unstarted-process-dialog.component';
import { TodosComponent } from './todos/todos.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UnapprovedProcessesComponent } from './unapproved-processes/unapproved-processes.component';
import { UnapprovedProcessDialogComponent } from './unapproved-process-dialog/unapproved-process-dialog.component';
import { PerformancerecordsModule } from '../performancerecords/performancerecords.module';
import { CeoApprovalDialogComponent } from './ceo-approval-dialog/ceo-approval-dialog.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ReactiveFormsModule } from '@angular/forms';
import { ApproveBySalesmanDialogComponent } from './approve-by-salesman-dialog/approve-by-salesman-dialog.component';
import { MatBadgeModule } from '@angular/material/badge';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { AllSoldItemsComponent } from './home-dashboard/all-sold-items/all-sold-items.component';
import { MatSelectModule } from '@angular/material/select';
import { SoldItemsByCompanyComponent } from './home-dashboard/sold-items-by-company/sold-items-by-company.component';
import { ChartsModule } from 'ng2-charts';
import { AverageSocialScoresComponent } from './home-dashboard/average-social-scores/average-social-scores.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
    declarations: [
        SearchbarComponent,
        OpenworkflowsComponent,
        TodoDialogComponent,
        UnstartedProcessDialogComponent,
        TodosComponent,
        EmployeeListComponent,
        UnapprovedProcessesComponent,
        UnapprovedProcessDialogComponent,
        CeoApprovalDialogComponent,
        ApproveBySalesmanDialogComponent,
        HomeDashboardComponent,
        AllSoldItemsComponent,
        ApproveBySalesmanDialogComponent,
        SoldItemsByCompanyComponent,
        AverageSocialScoresComponent
    ],
    exports: [
        SearchbarComponent,
        OpenworkflowsComponent,
        EmployeeListComponent,
        UnapprovedProcessesComponent,
        HomeDashboardComponent,
    ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatTableModule,
        MatIconModule,
        RouterModule,
        MatTooltipModule,
        PerformancerecordsModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatBadgeModule,
        MatSelectModule,
        ChartsModule,
        MatProgressBarModule
    ],
    entryComponents: [
        TodoDialogComponent,
        UnstartedProcessDialogComponent,
        UnapprovedProcessDialogComponent,
        CeoApprovalDialogComponent,
        ApproveBySalesmanDialogComponent
    ]
})
export class HomeModule {
}
