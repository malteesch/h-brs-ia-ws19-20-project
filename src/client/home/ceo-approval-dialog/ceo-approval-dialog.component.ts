import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PerformanceRecord } from '../../app/model';
import { AppState } from '../../app/store/reducers';
import { Store } from '@ngrx/store';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { hasRole, selectAuth } from '../../auth/store/auth.selectors';
import { AuthState } from '../../auth/store/reducers';
import { giveFeedback, approveProcessCEO, approveProcessHR } from '../../app/store/actions/app.actions';
import { Role } from '../../auth/model';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-ceo-approval-dialog',
    templateUrl: './ceo-approval-dialog.component.html',
    styleUrls: ['./ceo-approval-dialog.component.scss']
})
export class CeoApprovalDialogComponent implements OnInit {

    feedback: FormControl;
    approved: FormControl;
    extra: FormControl;
    approvalForm: FormGroup;
    total: number;
    isCEO$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    isHR$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        @Inject(MAT_DIALOG_DATA) public process: PerformanceRecord,
        private store: Store<AppState>,
        private dialogRef: MatDialogRef<CeoApprovalDialogComponent>
    ) {
        this.store
            .select(hasRole, {requiredRoles: [Role.CEO]})
            .subscribe((hasPermission) => this.isCEO$.next(hasPermission));
        this.store
            .select(hasRole, {requiredRoles: [Role.HR]})
            .subscribe((hasPermission) => this.isHR$.next(hasPermission));
    }

    ngOnInit() {
        this.feedback = new FormControl(this.process.feedback ? this.process.feedback.text : '');
        this.extra = new FormControl(this.process.bonus.extraBonus);
        this.approved = new FormControl(false, Validators.requiredTrue);
        this.approvalForm = new FormGroup({
            approved: this.approved,
            feedback: this.feedback,
            extra: this.extra
        });
        this.total = this.process.bonus.calculatedBonus + this.process.bonus.extraBonus;
        this.extra.valueChanges.subscribe(val => this.total = this.process.bonus.calculatedBonus + val);
    }

    approveProcess() {
        if (this.isCEO$.value) {
            this.store.select(selectAuth).subscribe((state: AuthState) => {
                this.store.dispatch(approveProcessCEO({
                        employeeId: this.getSalesmanInfo(this.process.id),
                        performanceRecordId: this.process.id,
                        extra: this.extra.value,
                        feedback: {
                            text: this.feedback.value,
                            fromUser: {fullName: state.fullName, jobTitle: state.jobTitle}
                        }
                    }
                ));
            }).unsubscribe();
        } else if (this.isHR$.value) {
            this.store.dispatch(approveProcessHR({
                employeeId: this.getSalesmanInfo(this.process.id),
                performanceRecordId: this.process.id,
            }));
        }
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }

    getSalesmanInfo(id: number): number {
        const idString = id.toString(10);
        const employeeId = idString.substr(0, idString.length - 4);
        return parseInt(employeeId, 10);
    }
}
