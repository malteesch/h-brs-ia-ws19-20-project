import { AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { PerformanceRecord, Settings } from '../../../app/model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AppState } from '../../../app/store/reducers';
import { Store } from '@ngrx/store';
import { selectSettings } from '../../../app/store/app.selectors';

@Component({
    selector: 'app-average-social-scores',
    templateUrl: './average-social-scores.component.html',
    styleUrls: ['./average-social-scores.component.scss']
})
export class AverageSocialScoresComponent implements AfterViewInit, OnDestroy {

    settings$: BehaviorSubject<Array<Settings>> = new BehaviorSubject<Array<Settings>>([]);
    settingsSubscription: Subscription = new Subscription();

    @ViewChild('primary', {static: true})
    primary: ElementRef;

    @ViewChild('secondary', {static: true})
    secondary: ElementRef;

    @Input()
    performanceRecords$: BehaviorSubject<Array<PerformanceRecord>>;
    performanceRecordSubscription = new Subscription();

    @Input()
    year$: BehaviorSubject<number>;
    yearSubscription = new Subscription();

    public lineChartData: ChartDataSets[] = [
        {data: [], label: 'Actual'},
    ];
    public lineChartLabels: Label[] = [
        'Leadership',
        'Openness',
        'Social Behaviour',
        'Attitude',
        'Communication',
        'Integrity'
    ];
    public lineChartOptions: ChartOptions = {};
    public lineChartLegend = true;

    constructor(private store: Store<AppState>) {
    }

    ngAfterViewInit() {

        this.settingsSubscription = this.store.select(selectSettings).subscribe(settings => {
            this.settings$.next(settings);
            this.performanceRecords$.next(this.performanceRecords$.value);
            this.yearSubscription = this.year$.subscribe(year => this.year$.next(year));
        });

        this.performanceRecordSubscription = this.performanceRecords$.subscribe(records => {
            const primaryColor = getComputedStyle(this.primary.nativeElement).color;
            const secondaryColor = getComputedStyle(this.secondary.nativeElement).color;
            const primaryLessOpacity = `rgba(${primaryColor.substring(4, primaryColor.length - 1)}, 0.2)`;
            const secondaryLessOpacity = `rgba(${secondaryColor.substring(4, secondaryColor.length - 1)}, 0.2)`;
            this.lineChartData = [];
            if (this.settings$.value.length > 0) {
                const targetValues =
                    this.settings$.value
                        .find(setting => setting.yearOfValidity === this.year$.value)
                        .socialPerformanceTargetValues
                        .map(obj => obj.targetValue);
                this.lineChartData.push({
                    data: targetValues,
                    label: 'Target Values',
                    backgroundColor: secondaryLessOpacity,
                    borderColor: secondaryColor,
                    pointBackgroundColor: secondaryColor,
                    pointBorderColor: secondaryColor
                });
                if (records.length > 0) {
                    let avgActualValues = [0, 0, 0, 0, 0, 0];
                    records.forEach(record => {
                        record.socialScores.forEach(score => {
                            avgActualValues[score.id - 1] += score.actualValue;
                        });
                    });
                    avgActualValues = avgActualValues.map(val => val / records.length);
                    this.lineChartData.push({
                        data: avgActualValues,
                        label: 'Average Actual Values',
                        backgroundColor: primaryLessOpacity,
                        borderColor: primaryColor,
                        pointBackgroundColor: primaryColor,
                        pointBorderColor: primaryColor
                    });
                }
                this.lineChartOptions = {
                    responsive: true,
                    aspectRatio: 3.2,
                    legend: {
                        labels: {
                            fontFamily: 'Montserrat'
                        }
                    },
                    scales: {
                        // We use this empty structure as a placeholder for dynamic theming.
                        xAxes: [{}],
                        yAxes: [{ticks: {beginAtZero: true, suggestedMax: 10}}]
                    },
                };
            }
        });
    }

    ngOnDestroy(): void {
        this.settingsSubscription.unsubscribe();
        this.performanceRecordSubscription.unsubscribe();
    }
}
