import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-all-sold-items',
    templateUrl: './all-sold-items.component.html',
    styleUrls: ['./all-sold-items.component.scss']
})
export class AllSoldItemsComponent implements OnInit {

    @Input()
    values: Array<{ label: string, value: number }>;

    data: Array<{ label: string, value: number }> = [];

    constructor() {
    }

    ngOnInit() {
    }

    mapValues(values) {
        const hooverGo = {label: 'HooverGo', value: 0};
        const hooverClean = {label: 'HooverClean', value: 0};
        values.forEach(obj => {
            if (obj.label === 'HooverGo') {
                hooverGo.value += obj.value;
            } else {
                hooverClean.value += obj.value;
            }
        });
        return [hooverGo, hooverClean];
    }

    fullCount(records: Array<any>) {
        return records.reduce((acc, record) => {
            return acc += record.value;
        }, 0);
    }
}
