import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { BehaviorSubject } from 'rxjs';
import { Order } from '../../../app/model';

@Component({
    selector: 'app-sold-items-by-company',
    templateUrl: './sold-items-by-company.component.html',
    styleUrls: ['./sold-items-by-company.component.scss']
})
export class SoldItemsByCompanyComponent implements OnInit {

    @ViewChild('primary', {static: true})
    primary: ElementRef;

    @ViewChild('secondary', {static: true})
    secondary: ElementRef;

    @Input()
    values: BehaviorSubject<Array<Order>>;

    options: ChartOptions = {};
    colors$ = new BehaviorSubject<any>([]);

    public barChartLabels: Label[] = [];
    public barChartLegend = true;

    public barChartData: ChartDataSets[] = [];

    constructor() {
    }

    ngOnInit() {
        this.values.subscribe((orders) => {
            const customers = [
                ...new Set(orders.sort((a, b) => a.customer.rating - b.customer.rating).map(order => {
                    return order.customer.name;
                }))
            ];
            const hooverGo: number[] = [];
            const hooverClean: number[] = [];
            this.barChartLabels = customers;
            orders.forEach(order => {
                order.positions.forEach(position => {
                    if (position.product.name === 'HooverGo') {
                        if (hooverGo[this.barChartLabels.indexOf(order.customer.name)] === undefined) {
                            hooverGo[this.barChartLabels.indexOf(order.customer.name)] = position.amount;
                        } else {
                            hooverGo[this.barChartLabels.indexOf(order.customer.name)] += position.amount;
                        }
                    } else {
                        if (hooverClean[this.barChartLabels.indexOf(order.customer.name)] === undefined) {
                            hooverClean[this.barChartLabels.indexOf(order.customer.name)] = position.amount;
                        } else {
                            hooverClean[this.barChartLabels.indexOf(order.customer.name)] += position.amount;
                        }
                    }
                });
            });
            this.barChartData = [
                {
                    data: hooverGo,
                    label: 'HooverGo',
                    backgroundColor: getComputedStyle(this.primary.nativeElement).color,
                    hoverBackgroundColor: getComputedStyle(this.primary.nativeElement).color
                },
                {
                    data: hooverClean,
                    label: 'HooverClean',
                    backgroundColor: getComputedStyle(this.secondary.nativeElement).color,
                    hoverBackgroundColor: getComputedStyle(this.secondary.nativeElement).color
                }
            ];


            this.options = {
                responsive: true,
                // We use these empty structures as placeholders for dynamic theming.
                scales: {xAxes: [{}], yAxes: [{ticks: {beginAtZero: true}}]},
                aspectRatio: 3,
                legend: {
                    labels: {
                        fontSize: 14,
                        fontFamily: 'Montserrat',
                    }
                }
            };
        });
    }
}
