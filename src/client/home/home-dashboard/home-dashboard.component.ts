import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { selectAllOrders, selectAllPerformanceRecords } from '../../app/store/app.selectors';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Order, PerformanceRecord } from '../../app/model';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-home-dashboard',
    templateUrl: './home-dashboard.component.html',
    styleUrls: ['./home-dashboard.component.scss']
})
export class HomeDashboardComponent implements OnInit, OnDestroy {
    availableYears$ = new BehaviorSubject<Array<number>>([]);
    availableYearsSubscription = new Subscription();
    allOrders$ = new BehaviorSubject<Array<Order>>([]);
    allPerformanceRecords$ = new BehaviorSubject<Array<PerformanceRecord>>([]);
    allPerformanceRecordsSubscription = new Subscription();
    years: FormControl = new FormControl();

    allOrdersSubscription = new Subscription();

    allSoldItems$: BehaviorSubject<Array<{ label: string, value: number; }>> = new BehaviorSubject([]);
    allOrdersByYear$: BehaviorSubject<Array<Order>> = new BehaviorSubject([]);
    allPerformanceRecordsByYear$: BehaviorSubject<Array<PerformanceRecord>> = new BehaviorSubject([]);

    year$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
    yearSubscription = new Subscription();

    constructor(private store: Store<AppState>) {
        this.allOrdersSubscription = this.store.select(selectAllOrders).subscribe(orders => {
            this.allOrders$.next(orders);
            this.availableYears$.next([...new Set(orders.map(order => new Date(order.date).getFullYear()))].sort((
                a,
                b
            ) => b - a));
        });
        this.allPerformanceRecordsSubscription = this.store.select(selectAllPerformanceRecords).subscribe(records => {
            this.allPerformanceRecords$.next(records);
        });
    }

    ngOnInit() {
        this.years.valueChanges.subscribe(year => {
            const result: Array<{ label: string, value: number }> = [];
            this.allOrders$.value
                .filter(order => year === new Date(order.date).getFullYear())
                .forEach(order => {
                    order.positions.forEach(position => {
                        result.push({label: position.product.name, value: position.amount});
                    });
                });
            this.allSoldItems$.next(result);
            this.allOrdersByYear$.next(this.allOrders$.value.filter(order => year === new Date(order.date).getFullYear()));
            this.allPerformanceRecordsByYear$.next(this.allPerformanceRecords$.value.filter(record => year === record.year));
            this.year$.next(year);
        });

        this.allOrdersSubscription = this.availableYears$.subscribe(years => {
            this.years.setValue(years[0]);
        });
    }

    ngOnDestroy(): void {
        this.allOrdersSubscription.unsubscribe();
        this.availableYearsSubscription.unsubscribe();
        this.allPerformanceRecordsSubscription.unsubscribe();
        this.yearSubscription.unsubscribe();
    }

}
