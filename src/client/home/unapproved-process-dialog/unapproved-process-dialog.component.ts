import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PerformanceRecord, SalesManProfile } from '../../app/model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { selectSalesmanById } from '../../app/store/app.selectors';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { CeoApprovalDialogComponent } from '../ceo-approval-dialog/ceo-approval-dialog.component';
import { HttpClient } from '@angular/common/http';
import { AuthState } from '../../auth/store/reducers';
import { hasRole } from '../../auth/store/auth.selectors';
import { Role } from '../../auth/model';

@Component({
    selector: 'app-unapproved-process-dialog',
    templateUrl: './unapproved-process-dialog.component.html',
    styleUrls: ['./unapproved-process-dialog.component.scss']
})
export class UnapprovedProcessDialogComponent implements OnInit, OnDestroy {

    isCEO$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    isHR$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    salesman$: Observable<SalesManProfile>;
    ceoApprovalDialogRefClosed: Subscription = new Subscription();

    constructor(
        @Inject(MAT_DIALOG_DATA) public process: PerformanceRecord,
        private store: Store<AppState>,
        private dialog: MatDialog,
        private dialogRef: MatDialogRef<UnapprovedProcessDialogComponent>
    ) {
        this.store
            .select(hasRole, {requiredRoles: [Role.CEO]})
            .subscribe((hasPermission) => this.isCEO$.next(hasPermission));
        this.store
            .select(hasRole, {requiredRoles: [Role.HR]})
            .subscribe((hasPermission) => this.isHR$.next(hasPermission));
    }

    ngOnInit() {
        this.salesman$ = this.getSalesmanInfo(this.process.id);
    }

    getSalesmanInfo(id: number): Observable<SalesManProfile> {
        const idString = id.toString(10);
        const employeeId = idString.substr(0, idString.length - 4);
        return this.store.select(selectSalesmanById(parseInt(employeeId, 10)));
    }

    openApproveProcess() {
        const dialogRef = this.dialog.open(CeoApprovalDialogComponent, {
            data: this.process,
            autoFocus: false,
            minWidth: '800px'
        });
        this.ceoApprovalDialogRefClosed = dialogRef.afterClosed().subscribe(approved => {
            if (approved) {
                this.dialogRef.close();
            }
        });
    }

    ngOnDestroy(): void {
        this.ceoApprovalDialogRefClosed.unsubscribe();
    }
}
