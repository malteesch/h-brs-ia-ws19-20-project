import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { Observable, of } from 'rxjs';
import { PerformanceRecord, ProcessStatus, SalesManProfile } from '../../app/model';
import { selectSalesmanById } from '../../app/store/app.selectors';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { UnapprovedProcessDialogComponent } from '../unapproved-process-dialog/unapproved-process-dialog.component';

@Component({
    selector: 'app-unapproved-processes',
    templateUrl: './unapproved-processes.component.html',
    styleUrls: ['./unapproved-processes.component.scss']
})
export class UnapprovedProcessesComponent implements OnInit, AfterViewInit {

    @Input()
    unApproved$: Observable<Array<PerformanceRecord>>;
    hasUnapproved$: Observable<boolean>;

    constructor(private store: Store<AppState>, private dialog: MatDialog) {
    }

    ngOnInit() {
        this.hasUnapproved$ = this.unApproved$.pipe(
            map(unapproved => unapproved.length > 0)
        );
    }

    getSalesmanInfo(id: number): Observable<SalesManProfile> {
        const idString = id.toString(10);
        const employeeId = idString.substr(0, idString.length - 4);
        return this.store.select(selectSalesmanById(parseInt(employeeId, 10)));
    }

    openUnapprovedDialog(process: PerformanceRecord) {
        this.dialog.open(UnapprovedProcessDialogComponent, {
            data: process,
            autoFocus: false,
            maxWidth: '1000px'
        });
    }

    ngAfterViewInit(): void {
    }

    determineProgress(status: ProcessStatus): number {
        let progress = 0;
        if (status.started) {
            progress += 25;
        }
        if (status.approvedByCEO) {
            progress += 25;
        }
        if (status.approvedByHR) {
            progress += 25;
        }
        if (status.approvedByEmployee) {
            progress += 25;
        }
        return progress;
    }
}
