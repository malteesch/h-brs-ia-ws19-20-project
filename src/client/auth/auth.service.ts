import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginDto } from './model';
import { environment } from '../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    login(dto: LoginDto): Observable<any> {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
            ['Content-Type']: 'application/json',
        });
        return this.http.post(`${environment.backendBaseUrl}/api/auth/login`, {
            username: dto.username,
            password: dto.password
        }, {
            headers,
        });
    }
}
