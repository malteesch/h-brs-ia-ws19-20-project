import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { loginAttempt } from '../store/auth.actions';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    username: FormControl;
    password: FormControl;
    loginForm: FormGroup;

    constructor(private store: Store<AppState>, private title: Title) {
        this.username = new FormControl('', Validators.required);
        this.password = new FormControl('', Validators.required);
        this.loginForm = new FormGroup({
            username: this.username,
            password: this.password,
        });
    }

    sendLoginData() {
        this.store.dispatch(loginAttempt(this.loginForm.value));
    }

    ngOnInit(): void {
        this.title.setTitle('Login | Performance Cockpit');
    }
}
