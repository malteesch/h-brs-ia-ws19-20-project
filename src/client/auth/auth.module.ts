import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { authReducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/auth.effects';
import { LoginComponent } from './login/login.component';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthRoutingModule } from './auth-routing.module';

export function tokenGetter() {
    return localStorage.getItem('accessToken');
}

@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        StoreModule.forFeature('auth', authReducers),
        EffectsModule.forFeature([AuthEffects]),
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        JwtModule.forRoot({
            config: {
                tokenGetter,
                whitelistedDomains: [
                    'localhost:3000',
                ],
            },
        }),
    ],
})
export class AuthModule {
}
