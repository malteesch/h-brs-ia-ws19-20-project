import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { isAuthenticated } from '../store/auth.selectors';
import { tap } from 'rxjs/operators';
import { setRedirectUrl } from '../store/auth.actions';
import { AuthState } from '../store/reducers';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivateChild {

    constructor(private store: Store<AuthState>, private router: Router) {
    }

    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.select(isAuthenticated).pipe(
            tap(async isLoggedId => {
                if (!isLoggedId) {
                    this.store.dispatch(setRedirectUrl({url: state.url}));
                    await this.router.navigate(['/login']);
                }
            }),
        );
    }
}
