import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthState } from '../store/reducers';
import { selectAuth } from '../store/auth.selectors';
import { map, tap } from 'rxjs/operators';
import { NotificationService } from '../../notification/notification.service';

@Injectable({
    providedIn: 'root'
})
export class OnlySalesmanGuard implements CanActivate {


    constructor(
        private store: Store<AuthState>,
        private router: Router,
        private notify: NotificationService) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.select(selectAuth).pipe(
            tap(async (auth: AuthState) => {
                if (auth.roles.length === 1 && auth.roles[0] === 'SALESMAN') {
                    await this.router.navigate(['/profile', auth.id]);
                    this.notify.info('You are only allowed to view your own profile');
                }
            }),
            map((auth: AuthState) => !(auth.roles.length === 1 && auth.roles[0] === 'SALESMAN')),
        );
    }

}
