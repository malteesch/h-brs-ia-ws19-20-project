import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthModule } from '../auth.module';
import { Store } from '@ngrx/store';
import { AuthState } from '../store/reducers';
import { tap } from 'rxjs/operators';
import { NotificationService } from '../../notification/notification.service';
import { AuthService } from '../auth.service';
import { hasRole } from '../store/auth.selectors';

@Injectable({
    providedIn: AuthModule
})
export class RoleGuard implements CanActivate {

    constructor(private store: Store<AuthState>, private notification: NotificationService, private authService: AuthService) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.select(hasRole, {requiredRoles: route.data.requiredRoles}).pipe(
            tap((hasPermission: boolean) => {
                if (!hasPermission) {
                    this.notification.error('Insufficient permissions to view this page');
                }
                return hasPermission;
            })
        );
    }
}
