import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    Router,
    RouterStateSnapshot,
    UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { isAuthenticated } from '../store/auth.selectors';
import { map, tap } from 'rxjs/operators';
import { AuthState } from '../store/reducers';

@Injectable({
    providedIn: 'root',
})
export class LoggedInGuard implements CanActivate {

    constructor(private store: Store<AuthState>, private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.select(isAuthenticated).pipe(
            map(isLoggedIn => !isLoggedIn),
            tap(async isLoggedOut => {
                if (!isLoggedOut) {
                    await this.router.navigate(['/home']);
                }
            }),
        );
    }
}
