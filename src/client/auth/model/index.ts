export interface CurrentUserDto {
    id: number;
    username: string;
    roles: Array<Role>;
    fullName: string;
    jobTitle: string;
}

export interface LoginDto {
    username: string;
    password: string;
}

export enum Role {
    ADMIN = 'ADMIN',
    CEO = 'CEO',
    HR = 'HR',
    SALESMAN = 'SALESMAN'
}
