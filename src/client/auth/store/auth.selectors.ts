import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { AuthState } from './reducers';
import { Role } from '../model';

export const selectAuth: MemoizedSelector<object, AuthState> = createFeatureSelector('auth');

export const selectUsername: MemoizedSelector<AuthState, string> = createSelector(
    selectAuth,
    (state: AuthState) => (state.username ? state.username : ''),
);

export const isAuthenticated: MemoizedSelector<AuthState, boolean> = createSelector(
    selectAuth,
    (state: AuthState) => !!state.username,
);

export const selectRoles = createSelector(
    selectAuth,
    (state: AuthState) => state.roles
);

export const currentRedirectUrl = createSelector(
    selectAuth,
    (state: AuthState) => state.redirectUrl,
);

export const hasRole = createSelector(
    selectRoles,
    (
        state: Array<Role>,
        props: { requiredRoles: Array<Role> }
    ) => props.requiredRoles.filter(role => state.includes(role)).length > 0
);

