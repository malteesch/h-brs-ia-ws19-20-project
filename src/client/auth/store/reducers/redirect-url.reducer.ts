import {Action, ActionReducer, createReducer, on} from '@ngrx/store';
import {resetRedirectUrl, setRedirectUrl} from '../auth.actions';

const redirectUrlReducer = createReducer(
    '/home',
    on(setRedirectUrl, (state: string, payload: { url: string }) => payload.url),
    on(resetRedirectUrl, () => '/home'),
);

export function reducer(state: string | undefined, action: Action) {
    return redirectUrlReducer(state, action);
}
