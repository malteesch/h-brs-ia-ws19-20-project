import { Action, createReducer, on } from '@ngrx/store';
import { loadTokenSuccessful, loginFailed, loginSuccessful, logoutFailed, logoutSuccessful } from '../auth.actions';
import { CurrentUserDto } from '../../model';

const rolesReducer = createReducer(
    [],
    on(loginSuccessful, (state, payload: CurrentUserDto) => payload.roles),
    on(loadTokenSuccessful, (state, payload: CurrentUserDto) => payload.roles),
    on(loginFailed, () => []),
    on(logoutSuccessful, () => []),
    on(logoutFailed, state => state),
);

export function reducer(state: Array<string> | undefined, action: Action) {
    return rolesReducer(state, action);
}
