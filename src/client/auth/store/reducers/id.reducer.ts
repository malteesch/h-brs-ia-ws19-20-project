import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import { loadTokenSuccessful, loginFailed, loginSuccessful, logoutFailed, logoutSuccessful } from '../auth.actions';
import { CurrentUserDto } from '../../model';

const idReducer = createReducer(
    null,
    on(loginSuccessful, (state, payload: CurrentUserDto) => payload.id),
    on(loadTokenSuccessful, (state, payload: CurrentUserDto) => payload.id),
    on(loginFailed, () => null),
    on(logoutSuccessful, () => null),
    on(logoutFailed, state => state),
);

export function reducer(state: number | undefined, action: Action) {
    return idReducer(state, action);
}
