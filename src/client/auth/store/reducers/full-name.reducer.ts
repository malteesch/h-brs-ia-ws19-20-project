import { Action, createReducer, on } from '@ngrx/store';
import { loadTokenSuccessful, loginFailed, loginSuccessful, logoutFailed, logoutSuccessful } from '../auth.actions';
import { CurrentUserDto } from '../../model';

const fullNameReducer = createReducer(
    null,
    on(loginSuccessful, (state, payload: CurrentUserDto) => payload.fullName),
    on(loadTokenSuccessful, (state, payload: CurrentUserDto) => payload.fullName),
    on(loginFailed, () => null),
    on(logoutSuccessful, () => null),
    on(logoutFailed, state => state),
);

export function reducer(state: string, action: Action) {
    return fullNameReducer(state, action);
}
