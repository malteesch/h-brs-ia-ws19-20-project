import { Action, createReducer, on } from '@ngrx/store';
import { loadTokenSuccessful, loginFailed, loginSuccessful, logoutFailed, logoutSuccessful } from '../auth.actions';
import { CurrentUserDto } from '../../model';

const jobTitleReducer = createReducer(
    null,
    on(loginSuccessful, (state, payload: CurrentUserDto) => payload.jobTitle),
    on(loadTokenSuccessful, (state, payload: CurrentUserDto) => payload.jobTitle),
    on(loginFailed, () => null),
    on(logoutSuccessful, () => null),
    on(logoutFailed, state => state),
);

export function reducer(state: string, action: Action) {
    return jobTitleReducer(state, action);
}
