import { ActionReducer, Action, createReducer, on } from '@ngrx/store';
import { loadTokenSuccessful, loginFailed, loginSuccessful, logoutFailed, logoutSuccessful } from '../auth.actions';
import { CurrentUserDto } from '../../model';

const usernameReducer = createReducer(
    null,
    on(loginSuccessful, (state, payload: CurrentUserDto) => payload.username),
    on(loadTokenSuccessful, (state, payload: CurrentUserDto) => payload.username),
    on(loginFailed, () => null),
    on(logoutSuccessful, () => null),
    on(logoutFailed, state => state),
);

export function reducer(state: string | undefined, action: Action) {
    return usernameReducer(state, action);
}
