import { ActionReducerMap } from '@ngrx/store';
import * as roles from './roles.reducer';
import * as username from './username.reducer';
import { Role } from '../../model';
import * as id from './id.reducer';
import * as redirectUrl from './redirect-url.reducer';
import * as fullName from './full-name.reducer';
import * as jobTitle from './job-title.reducer';

export interface AuthState {
    id: number;
    username: string;
    fullName: string;
    jobTitle: string;
    roles: Array<Role>;
    redirectUrl: string;
}

export const authReducers: ActionReducerMap<AuthState> = {
    id: id.reducer,
    username: username.reducer,
    fullName: fullName.reducer,
    jobTitle: jobTitle.reducer,
    roles: roles.reducer,
    redirectUrl: redirectUrl.reducer
};
