import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
    loadToken,
    loadTokenFailed,
    loadTokenSuccessful,
    loginAttempt,
    loginFailed,
    loginSuccessful,
    logoutAttempt,
    logoutFailed,
    logoutSuccessful,
    resetRedirectUrl
} from './auth.actions';
import { catchError, exhaustMap, map, mergeAll, mergeMap, switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { AppState } from '../../app/store/reducers';
import { Store } from '@ngrx/store';
import { currentRedirectUrl } from './auth.selectors';
import { loadSalesmen, loadSettings } from '../../app/store/actions/app.actions';
import { NotificationService } from '../../notification/notification.service';

@Injectable()
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private jwt: JwtHelperService,
        private router: Router,
        private store: Store<AppState>,
        private notify: NotificationService,
    ) {
    }

    loadTokenSuccessful$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loadTokenSuccessful),
        mergeMap(() => of([loadSalesmen(), loadSettings()])),
        mergeAll(),
    ));

    attemptLogin$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loginAttempt),
        exhaustMap(action => this.authService.login(action).pipe(
            tap(res => {
                localStorage.setItem('accessToken', res.accessToken);
            }),
            map(res => {
                const accessToken = res.accessToken;
                const {username, roles, id, fullName, jobTitle} = this.jwt.decodeToken(accessToken);
                return loginSuccessful({
                    username,
                    roles,
                    id,
                    fullName,
                    jobTitle
                });
            }),
            catchError(err => of(loginFailed({message: err.error.message}))),
        )),
    ));

    checkToken$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loadToken),
        map(() => {
            const accessToken = localStorage.getItem('accessToken');
            if (accessToken) {
                const {username, exp, roles, id, fullName, jobTitle} = this.jwt.decodeToken(accessToken);
                if (exp * 1000 < new Date().getTime()) {    // getTime gives milliseconds
                    throw Error();
                }
                return loadTokenSuccessful({
                    username,
                    roles,
                    id,
                    fullName,
                    jobTitle
                });
            }
            return loadTokenFailed();
        }),
        catchError(err => of(loadTokenFailed())),
    ));

    attempLogout$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(logoutAttempt),
        tap(() => {
            localStorage.removeItem('accessToken');
        }),
        map(() => logoutSuccessful()),
        catchError(err => of(logoutFailed())),
    ));

    loginSuccessful$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loginSuccessful),
        switchMap(async () => {
            this.store.select(currentRedirectUrl)
                .subscribe(async redirectUrl => await this.router.navigate([redirectUrl]))
                .unsubscribe();
            return resetRedirectUrl();
        }),
        mergeMap(() => of([loadSalesmen(), loadSettings()])),
        mergeAll()
    ));

    logoutSuccess$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(logoutSuccessful),
        tap(async () => {
            await this.router.navigate(['/login']);
            this.notify.done('Logged out');
        }),
    ), {dispatch: false});

    loginFailed$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loginFailed),
        tap(action => {
            this.notify.error(action.message);
        }),
    ), {dispatch: false});
}
