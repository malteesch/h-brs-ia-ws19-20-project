import { ActionCreator, createAction, props } from '@ngrx/store';
import { TypedAction } from '@ngrx/store/src/models';
import { CurrentUserDto, LoginDto } from '../model';

export const loginAttempt = createAction(
    '[AUTH] Login attempt',
    props<LoginDto>(),
);

export const loginSuccessful = createAction(
    '[AUTH] Login successful',
    props<CurrentUserDto>(),
);

export const loginFailed = createAction(
    '[AUTH] Login failed',
    props<{ message: string }>()
);

export const logoutAttempt = createAction(
    '[AUTH] Logout attempt',
);

export const logoutSuccessful = createAction(
    '[AUTH] Logout successful',
);

export const logoutFailed = createAction(
    '[AUTH] Logout failed',
);

export const loadToken = createAction(
    '[AUTH] Load token',
);

export const loadTokenSuccessful = createAction(
    '[AUTH] Load token successful',
    props<CurrentUserDto>()
);

export const loadTokenFailed = createAction(
    '[AUTH] Load token failed'
);

export const setRedirectUrl = createAction(
    '[AUTH] Set redirect URL',
    props<{url: string}>()
);

export const resetRedirectUrl = createAction(
    '[AUTH] Reset redirect URL'
);
