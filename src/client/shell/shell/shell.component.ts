import { Component, OnInit } from '@angular/core';
import { Theme } from '../../themes/model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { changeTheme } from '../../themes/store/themes.actions';

@Component({
    selector: 'app-shell',
    templateUrl: './shell.component.html',
    styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {

    Theme = Theme;

    constructor(private store: Store<AppState>) {
    }

    ngOnInit() {
    }

    changeTheme(theme: Theme) {
        this.store.dispatch(changeTheme({theme}));
    }
}
