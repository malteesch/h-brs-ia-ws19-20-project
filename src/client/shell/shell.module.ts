import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutDialogComponent, MenuComponent } from './menu/menu.component';
import { ShellComponent } from './shell/shell.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule, MatRippleModule, MatSlideToggleModule } from '@angular/material';
import { MatBadgeModule } from '@angular/material/badge';

@NgModule({
    declarations: [MenuComponent, ShellComponent, LogoutDialogComponent],
    imports: [
        CommonModule,
        MatToolbarModule,
        RouterModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatRippleModule,
        MatBadgeModule,
    ],
    entryComponents: [
        LogoutDialogComponent,
    ]
})
export class ShellModule {
}
