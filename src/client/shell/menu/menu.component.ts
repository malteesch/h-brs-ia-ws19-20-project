import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { logoutAttempt } from '../../auth/store/auth.actions';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Theme } from '../../themes/model';
import { hasRole, selectAuth, selectRoles } from '../../auth/store/auth.selectors';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Role } from '../../auth/model';
import { selectUnapprovedByCEO, selectUnapprovedByHR, selectUnstartedProcesses } from '../../app/store/app.selectors';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSlideToggle } from '@angular/material/slide-toggle';


@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, OnDestroy {

    Theme = Theme;

    isHandset$: Observable<boolean> = this.breakpoints.observe('(max-width: 576px)').pipe(
        map(result => result.matches),
    );

    @ViewChild('drawer', {static: true})
    sidenav: MatSidenav;

    @ViewChild('slideToggle', {static: true})
    slideToggle: MatSlideToggle;

    currentUser: Observable<string> = this.store.select(selectAuth).pipe(
        map(authState => authState.fullName)
    );

    hasSettingsPermission$: Observable<boolean> = this.store.select(hasRole, {
        requiredRoles: [
            Role.CEO,
            Role.HR,
            Role.ADMIN
        ]
    });
    hasOnlySalesmanPermission$: Observable<boolean> = this.store.select(selectRoles).pipe(
        map(roles => roles[0] === Role.SALESMAN && roles.length === 1)
    );
    isHR$: Observable<boolean> = this.store.select(hasRole, {requiredRoles: [Role.HR]});
    isCEO$: Observable<boolean> = this.store.select(hasRole, {requiredRoles: [Role.CEO]});

    unstartedCount$: BehaviorSubject<number> = new BehaviorSubject(0);
    unstartedSubscription: Subscription = new Subscription();

    unapprovedHRCount$: BehaviorSubject<number> = new BehaviorSubject(0);
    unapprovedHRSubscription: Subscription = new Subscription();

    unapprovedCEOCount$: BehaviorSubject<number> = new BehaviorSubject(0);
    unapprovedCEOSubscription: Subscription = new Subscription();

    constructor(
        private store: Store<AppState>,
        private breakpoints: BreakpointObserver,
        private dialog: MatDialog
    ) {
        this.unstartedSubscription = this.store
            .select(selectUnstartedProcesses)
            .subscribe(processes => this.unstartedCount$.next(processes.length));

        this.unapprovedHRSubscription = this.store
            .select(selectUnapprovedByHR)
            .subscribe(processes => this.unapprovedHRCount$.next(processes.length));

        this.unapprovedCEOSubscription = this.store
            .select(selectUnapprovedByCEO)
            .subscribe(processes => this.unapprovedCEOCount$.next(processes.length));
    }

    notificationCountHR(): number {
        return this.unstartedCount$.value + this.unapprovedHRCount$.value;
    }

    notificationCountCEO(): number {
        return this.unapprovedCEOCount$.value;
    }

    ngOnInit() {
    }

    logoutPrompt() {
        this.dialog.open(LogoutDialogComponent, {autoFocus: false});
    }

    ngOnDestroy(): void {
        this.unstartedSubscription.unsubscribe();
        this.unapprovedHRSubscription.unsubscribe();
        this.unapprovedCEOSubscription.unsubscribe();
    }
}

@Component({
    selector: 'app-logout-dialog',
    template: `
        <div mat-dialog-title>Logout?</div>
        <div class="button-container">
            <button mat-stroked-button (click)="dialogRef.close()">No</button>
            <button mat-stroked-button (click)="logout()">Yes</button>
        </div>
    `,
    styles: ['button {margin: 15px}'],
})
export class LogoutDialogComponent {
    constructor(
        public dialogRef: MatDialogRef<LogoutDialogComponent>,
        private store: Store<AppState>
    ) {
    }

    logout() {
        this.dialogRef.close();
        this.store.dispatch(logoutAttempt());
    }
}
