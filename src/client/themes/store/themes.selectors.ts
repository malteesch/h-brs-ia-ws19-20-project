import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { ThemeState } from './reducers';

const selectTheme: MemoizedSelector<object, ThemeState> = createFeatureSelector('theme');

export const currentTheme = createSelector(
    selectTheme,
    (state: ThemeState) => state.currentTheme,
);
