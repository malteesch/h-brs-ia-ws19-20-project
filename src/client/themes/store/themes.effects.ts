import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { changeTheme, loadTheme } from './themes.actions';
import { map, tap } from 'rxjs/operators';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Theme } from '../model';
import { ThemeService } from 'ng2-charts';

@Injectable()
export class ThemesEffects {

    changeTheme$: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(changeTheme),
        tap(action => {
            const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
            const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('-theme'));
            if (themeClassesToRemove.length) {
                overlayContainerClasses.remove(...themeClassesToRemove);
            }
            overlayContainerClasses.add(action.theme);
            let overrides;
            if (action.theme === 'dark-theme') {
                overrides = {
                    legend: {
                        labels: {fontColor: 'white'}
                    },
                    scales: {
                        xAxes: [
                            {
                                ticks: {fontColor: 'white'},
                                gridLines: {color: 'rgb(255,255,255)'}
                            }
                        ],
                        yAxes: [
                            {
                                ticks: {fontColor: 'white'},
                                gridLines: {color: 'rgb(255,255,255)'}
                            }
                        ]
                    }
                };
            } else {
                overrides = {};
            }
            this.chartTheme.setColorschemesOptions(overrides);
            localStorage.setItem('theme', action.theme);
        })
    ), {dispatch: false});

    loadTheme$: Observable<any> = createEffect(() => this.actions.pipe(
        ofType(loadTheme),
        map(() => {
            const theme: Theme = localStorage.getItem('theme') as Theme;
            return changeTheme({
                theme: theme === null ? Theme.DEFAULT : theme
            });
        })
    ));

    constructor(
        private actions: Actions,
        private overlayContainer: OverlayContainer,
        private chartTheme: ThemeService
    ) {
    }
}
