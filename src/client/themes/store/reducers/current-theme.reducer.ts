import { Action, createReducer, on } from '@ngrx/store';
import { changeTheme } from '../themes.actions';
import { Theme } from '../../model';

const currentThemeReducer = createReducer(
    Theme.DEFAULT,
    on(changeTheme, (state, payload) => {
        return payload.theme ? payload.theme : state;
    }),
);

export function reducer(state: Theme | undefined, action: Action) {
    return currentThemeReducer(state, action);
}
