import { ActionReducerMap } from '@ngrx/store';
import * as currentTheme from './current-theme.reducer';
import { Theme } from '../../model';

export interface ThemeState {
    currentTheme: Theme;
}

export const themeReducers: ActionReducerMap<ThemeState> = {
    currentTheme: currentTheme.reducer,
};
