import { ActionCreator, createAction, props } from '@ngrx/store';
import { TypedAction } from '@ngrx/store/src/models';
import { Theme } from '../model';

export const changeTheme = createAction(
    '[THEME] Change theme',
    props<{ theme: Theme }>(),
);

export const loadTheme = createAction(
    '[THEME] Load theme',
);

