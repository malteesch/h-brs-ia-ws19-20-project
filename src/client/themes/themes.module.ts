import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { themeReducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { ThemesEffects } from './store/themes.effects';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        StoreModule.forFeature('theme', themeReducers),
        EffectsModule.forFeature([ThemesEffects])
    ]
})
export class ThemesModule {
}
