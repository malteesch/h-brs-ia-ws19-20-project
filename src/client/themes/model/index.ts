export enum Theme {
    DEFAULT = 'default-theme',
    DARK = 'dark-theme',
    LIGHT = 'light-theme',
}
