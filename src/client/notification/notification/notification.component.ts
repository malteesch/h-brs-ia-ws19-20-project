import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

export interface NotificationData {
    message: string;
    icon: string;
    class: ColorClass;
}

export type ColorClass = 'error' | 'warn' | 'info' | 'done';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: NotificationData, private snackBarRef: MatSnackBarRef<NotificationComponent>) {
    }

    dismiss() {
        this.snackBarRef.dismiss();
    }

    ngOnInit() {
    }

}
