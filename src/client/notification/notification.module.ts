import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NotificationComponent } from './notification/notification.component';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [NotificationComponent],
    imports: [
        CommonModule,
        MatSnackBarModule,
        MatIconModule
    ],
    entryComponents: [NotificationComponent]
})
export class NotificationModule {
}
