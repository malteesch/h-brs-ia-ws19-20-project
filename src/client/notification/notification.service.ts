import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationComponent, NotificationData } from './notification/notification.component';
import { NotificationModule } from './notification.module';

@Injectable({
    providedIn: NotificationModule
})
export class NotificationService {

    constructor(private snackBar: MatSnackBar) {
    }

    error(message: string) {
        const data: NotificationData = {
            message,
            icon: 'error',
            class: 'error'
        };
        this.snackBar.openFromComponent(NotificationComponent, {
            data,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 3000
        });
    }

    warn(message: string) {
        const data: NotificationData = {
            message,
            icon: 'warning',
            class: 'warn'
        };
        this.snackBar.openFromComponent(NotificationComponent, {
            data,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 3000
        });
    }

    info(message: string) {
        const data: NotificationData = {
            message,
            icon: 'info',
            class: 'info'
        };
        this.snackBar.openFromComponent(NotificationComponent, {
            data,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 3000
        });
    }

    done(message: string) {
        const data: NotificationData = {
            message,
            icon: 'done',
            class: 'done'
        };
        this.snackBar.openFromComponent(NotificationComponent, {
            data,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 3000
        });
    }

}
