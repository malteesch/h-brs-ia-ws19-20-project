import { Component, Input, OnInit } from '@angular/core';
import { SocialScore } from '../../app/model';

@Component({
    selector: 'app-social-performance-details',
    templateUrl: './social-performance-details.component.html',
    styleUrls: ['./social-performance-details.component.scss']
})
export class SocialPerformanceDetailsComponent implements OnInit {

    displayedColumns: string[] = ['category', 'targetValue', 'actualValue', 'bonus'];
    total: number;
    @Input()
    socialScores: Array<SocialScore>;

    constructor() {
    }

    ngOnInit() {
        this.total = this.socialScores.reduce((acc, curr) => acc + curr.bonus, 0);
    }

}
