import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SalesScore } from '../../app/model';

@Component({
    selector: 'app-sales-score-details-dialog',
    templateUrl: './sales-score-details-dialog.component.html',
    styleUrls: ['./sales-score-details-dialog.component.scss']
})
export class SalesScoreDetailsDialogComponent implements OnInit {

    constructor(
        @Inject(MAT_DIALOG_DATA)
        public salesScores: Array<SalesScore>,
    ) {
    }

    ngOnInit() {
    }
}
