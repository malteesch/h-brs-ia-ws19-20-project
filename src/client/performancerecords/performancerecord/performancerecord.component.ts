import { Component, Input, OnInit } from '@angular/core';
import { PerformanceRecord, SalesScore } from '../../app/model';
import { MatDialog } from '@angular/material';
import { SocialPerformanceDetailsDialogComponent } from '../social-performance-details-dialog/social-performance-details-dialog.component';
import { SalesScoreDetailsDialogComponent } from '../sales-score-details-dialog/sales-score-details-dialog.component';

@Component({
    selector: 'app-performancerecord',
    templateUrl: './performancerecord.component.html',
    styleUrls: ['./performancerecord.component.scss']
})
export class PerformancerecordComponent implements OnInit {

    @Input()
    performanceRecord: PerformanceRecord;

    salesHooverGo: Array<SalesScore> = [];
    salesHooverClean: Array<SalesScore> = [];
    pieChartData: Array<{ label: string, value: number; }> = [];

    constructor(private dialog: MatDialog) {
    }

    ngOnInit() {
        this.performanceRecord.salesScores.forEach(score => {
            if (score.product.name === 'HooverGo') {
                this.salesHooverGo.push(score);
            } else {
                this.salesHooverClean.push(score);
            }
        });

        this.salesHooverGo.sort((a, b) => a.customer.rating - b.customer.rating);
        this.salesHooverClean.sort((a, b) => a.customer.rating - b.customer.rating);

        this.pieChartData = [
            {
                label: 'HooverGo',
                value: this.salesHooverGo.reduce<number>((sum, sale) => {
                    return sum + sale.itemsSold;
                }, 0)
            },
            {
                label: 'HooverClean',
                value: this.salesHooverClean.reduce<number>((sum, sale) => {
                    return sum + sale.itemsSold;
                }, 0)
            }
        ];
    }

    socialPerformanceRecordsClicked() {
        const dialogRef = this.dialog.open(SocialPerformanceDetailsDialogComponent, {
            data: this.performanceRecord.socialScores,
            width: '600px',
            autoFocus: false
        });
    }

    salesClicked(items: Array<SalesScore>) {
        const dialogRef = this.dialog.open(SalesScoreDetailsDialogComponent, {
            data: items,
            width: '600px',
            autoFocus: false
        });
    }
}
