import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SalesScore } from '../../app/model';

@Component({
    selector: 'app-sales-list',
    templateUrl: './sales-list.component.html',
    styleUrls: ['./sales-list.component.scss']
})
export class SalesListComponent implements OnInit {

    @Input()
    label: string;

    @Input()
    sales: Array<SalesScore>;

    @Output()
    salesItemsClicked: EventEmitter<Array<SalesScore>> = new EventEmitter<Array<SalesScore>>();

    constructor() {
    }

    ngOnInit() {
    }

    itemsClicked(sales: Array<SalesScore>) {
        this.salesItemsClicked.emit(sales);
    }
}
