import { Component, Input, OnInit } from '@angular/core';
import { SocialScore } from '../../app/model';

@Component({
    selector: 'app-socialperformance-list',
    templateUrl: './social-performance-list.component.html',
    styleUrls: ['./social-performance-list.component.scss']
})
export class SocialPerformanceListComponent implements OnInit {

    @Input()
    records: Array<SocialScore>;

    constructor() {
    }

    ngOnInit() {
    }

}
