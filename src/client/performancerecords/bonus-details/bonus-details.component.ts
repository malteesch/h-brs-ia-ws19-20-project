import { Component, Input, OnInit } from '@angular/core';
import { PerformanceRecord, SalesScore } from '../../app/model';

@Component({
    selector: 'app-bonus-details',
    templateUrl: './bonus-details.component.html',
    styleUrls: ['./bonus-details.component.scss']
})
export class BonusDetailsComponent implements OnInit {

    salesHooverGo: Array<SalesScore> = [];
    salesHooverClean: Array<SalesScore> = [];
    total: number;

    @Input() record: PerformanceRecord;

    constructor() {
    }

    ngOnInit() {
        this.record.salesScores.forEach(score => {
            if (score.product.name === 'HooverGo') {
                this.salesHooverGo.push(score);
            } else {
                this.salesHooverClean.push(score);
            }
        });
    }

    private calculateTotal() {
        return this.record.bonus.calculatedBonus + this.record.bonus.extraBonus;
    }
}
