import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-socialperformancescore',
    templateUrl: './social-performance-score.component.html',
    styleUrls: ['./social-performance-score.component.scss']
})
export class SocialPerformanceScoreComponent implements OnInit {

    @Input()
    label: string;

    @Input()
    difference: number;

    constructor() {
    }

    ngOnInit() {
    }

}
