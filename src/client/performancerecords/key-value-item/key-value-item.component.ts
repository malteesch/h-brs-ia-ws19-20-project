import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-key-value-item',
    templateUrl: './key-value-item.component.html',
    styleUrls: ['./key-value-item.component.scss']
})
export class KeyValueItemComponent implements OnInit {
    @Input()
    label: string;

    constructor() {
    }

    ngOnInit() {
    }

}
