import { Component, Input, OnInit } from '@angular/core';
import { SalesScore } from '../../app/model';

@Component({
    selector: 'app-sales-score-details',
    templateUrl: './sales-score-details.component.html',
    styleUrls: ['./sales-score-details.component.scss']
})
export class SalesScoreDetailsComponent implements OnInit {

    displayedColumns: string[] = ['companyName', 'companyRating', 'itemsSold', 'bonus'];
    total: number;
    @Input()
    salesScores: Array<SalesScore>;

    constructor() {
    }

    ngOnInit() {
        this.total = this.salesScores.reduce((acc, curr) => acc + curr.bonus, 0);
    }

}
