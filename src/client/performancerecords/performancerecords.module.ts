import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PiechartComponent } from './piechart/piechart.component';
import { SocialPerformanceListComponent } from './social-performance-list/social-performance-list.component';
import { SocialPerformanceScoreComponent } from './social-performance-score/social-performance-score.component';
import { MatButtonModule, MatDialogModule, MatIconModule, MatListModule, MatTableModule } from '@angular/material';
import { PerformancerecordComponent } from './performancerecord/performancerecord.component';
import { KeyValueItemComponent } from './key-value-item/key-value-item.component';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesScoreComponent } from './sales-score/sales-score.component';
import { SocialPerformanceDetailsDialogComponent } from './social-performance-details-dialog/social-performance-details-dialog.component';
import { SalesScoreDetailsDialogComponent } from './sales-score-details-dialog/sales-score-details-dialog.component';
import { SalesScoreDetailsComponent } from './sales-score-details/sales-score-details.component';
import { SocialPerformanceDetailsComponent } from './social-performance-details/social-performance-details.component';
import { BonusDetailsDialogComponent } from './bonus-details-dialog/bonus-details-dialog.component';
import { StartProcessDialogComponent } from './start-process-dialog/start-process-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BonusDetailsComponent } from './bonus-details/bonus-details.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
    declarations: [
        PiechartComponent,
        SocialPerformanceListComponent,
        SocialPerformanceScoreComponent,
        PerformancerecordComponent,
        KeyValueItemComponent,
        SalesListComponent,
        SalesScoreComponent,
        SocialPerformanceDetailsDialogComponent,
        SalesScoreDetailsDialogComponent,
        SalesScoreDetailsComponent,
        SocialPerformanceDetailsComponent,
        BonusDetailsDialogComponent,
        StartProcessDialogComponent,
        BonusDetailsComponent,
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        MatTableModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        ChartsModule
    ],
    exports: [
        PiechartComponent,
        SocialPerformanceListComponent,
        PerformancerecordComponent,
        KeyValueItemComponent,
        SalesScoreComponent,
        BonusDetailsComponent
    ],
    entryComponents: [
        SocialPerformanceDetailsDialogComponent,
        SalesScoreDetailsDialogComponent,
        BonusDetailsDialogComponent,
        StartProcessDialogComponent,
    ]
})
export class PerformancerecordsModule {
}
