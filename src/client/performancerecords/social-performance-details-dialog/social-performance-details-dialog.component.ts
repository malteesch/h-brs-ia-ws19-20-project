import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SocialScore } from '../../app/model';

@Component({
    selector: 'app-social-performance-details-dialog',
    templateUrl: './social-performance-details-dialog.component.html',
    styleUrls: ['./social-performance-details-dialog.component.scss']
})
export class SocialPerformanceDetailsDialogComponent implements OnInit {

    constructor(
        @Inject(MAT_DIALOG_DATA)
        public socialScores: Array<SocialScore>,
    ) {
    }

    ngOnInit() {
    }
}
