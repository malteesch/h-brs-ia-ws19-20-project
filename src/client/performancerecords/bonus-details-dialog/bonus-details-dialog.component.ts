import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PerformanceRecord } from '../../app/model';

@Component({
    selector: 'app-bonus-details-dialog',
    templateUrl: './bonus-details-dialog.component.html',
    styleUrls: ['./bonus-details-dialog.component.scss']
})
export class BonusDetailsDialogComponent implements OnInit {
    total: number;

    constructor(
        @Inject(MAT_DIALOG_DATA) public performanceRecord: PerformanceRecord
    ) {
    }

    ngOnInit() {
        this.total = this.calculateTotal();
    }

    private calculateTotal() {
        return this.performanceRecord.bonus.calculatedBonus + this.performanceRecord.bonus.extraBonus;
    }
}
