import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ChartOptions } from 'chart.js';
import { Store } from '@ngrx/store';
import { ThemeState } from '../../themes/store/reducers';
import { currentTheme } from '../../themes/store/themes.selectors';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-piechart',
    templateUrl: './piechart.component.html',
    styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {
    @ViewChild('primary', {static: true})
    primary: ElementRef;

    @ViewChild('secondary', {static: true})
    secondary: ElementRef;

    // tslint:disable-next-line:no-input-rename
    @Input('data')
    values: Array<{ label: string, value: number }>;

    options: ChartOptions = {};
    colors$ = new BehaviorSubject<any>([]);

    constructor(private store: Store<ThemeState>) {
    }

    getValues(dataObjects: Array<{ label: string; value: number }>) {
        return dataObjects.map(obj => obj.value);
    }

    getLabels(dataObjects: Array<{ label: string; value: number }>) {
        return dataObjects.map(obj => obj.label);
    }

    ngOnInit(): void {
        this.store.select(currentTheme).subscribe(theme => {
            const fontColor = theme === 'dark-theme' ? 'white' : 'black';
            const colors = this.values.map((set, index) => {
                if (index % 2 === 0) {
                    return getComputedStyle(this.primary.nativeElement).color;
                } else {
                    return getComputedStyle(this.secondary.nativeElement).color;
                }
            });
            this.colors$.next([{backgroundColor: colors, borderColor: colors}]);
            this.options = {
                responsive: true,
                legend: {
                    display: true,
                    position: 'right',
                    labels: {
                        fontFamily: 'Montserrat',
                        fontColor,
                    },
                },
            };
        });
    }
}
