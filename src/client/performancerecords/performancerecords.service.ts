import { Injectable } from '@angular/core';
import { PerformancerecordsModule } from './performancerecords.module';
import { Store } from '@ngrx/store';
import { AppState } from '../app/store/reducers';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { selectPerformanceRecord } from '../app/store/app.selectors';
import { Feedback, PerformanceRecord, StartProcessPayload } from '../app/model';

@Injectable({
    providedIn: PerformancerecordsModule
})
export class PerformancerecordsService {

    constructor(private store: Store<AppState>, private http: HttpClient) {
    }

    getPerformanceRecord(store: Store<AppState>, employeeId: number, recordId: number): PerformanceRecord {
        let performanceRecord: PerformanceRecord = null;
        const sub = store.select(selectPerformanceRecord(employeeId, recordId)).subscribe(perfRecord => {
            performanceRecord = perfRecord;
        });
        sub.unsubscribe();
        return performanceRecord;
    }

    startProcess(payload: StartProcessPayload): Observable<any> {

        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        return this.http.post(
            `${environment.backendBaseUrl}/api/salesmen/${payload.employeeId}/performancerecords/start`,
            {
                year: payload.year,
                socialPerformance: payload.socialPerformance
            },
            {headers}
        );
    }

    giveFeedback(payload: { employeeId: number, performanceRecordId: number, feedback: Feedback }) {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        return this.http.put(
            `${environment.backendBaseUrl}/api/salesmen/${payload.employeeId}/performancerecords/${payload.performanceRecordId}`,
            {
                processStatus: {
                    ...this.getPerformanceRecord(this.store, payload.employeeId, payload.performanceRecordId).processStatus,
                    feedbackGiven: true
                },
                feedback: payload.feedback
            },
            {headers}
        );
    }

    approveProcessCEO(payload: { employeeId: number, performanceRecordId: number, extra: number, feedback: Feedback }) {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        const body: any = {
            processStatus: {
                ...this.getPerformanceRecord(this.store, payload.employeeId, payload.performanceRecordId).processStatus,
                approvedByCEO: true
            },
            bonus: {
                ...this.getPerformanceRecord(this.store, payload.employeeId, payload.performanceRecordId).bonus,
                extraBonus: payload.extra,
            },
        };

        if (payload.feedback.text.length > 0) {
            body.feedback = payload.feedback;
            body.processStatus.feedbackGiven = true;
        }

        return this.http.put(
            `${environment.backendBaseUrl}/api/salesmen/${payload.employeeId}/performancerecords/${payload.performanceRecordId}`,
            body,
            {headers}
        );
    }

    approveProcessHR(payload: { employeeId: number, performanceRecordId: number }) {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        return this.http.put(
            `${environment.backendBaseUrl}/api/salesmen/${payload.employeeId}/performancerecords/${payload.performanceRecordId}`,
            {
                processStatus: {
                    ...this.getPerformanceRecord(this.store, payload.employeeId, payload.performanceRecordId).processStatus,
                    approvedByHR: true
                },
            },
            {headers}
        );
    }

    approveProcessSalesman(payload: { employeeId: number, performanceRecordId: number }) {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        return this.http.post(
            `${environment.backendBaseUrl}/api/salesmen/${payload.employeeId}/performancerecords/${payload.performanceRecordId}/approve`,
            {},
            {headers}
        );
    }
}
