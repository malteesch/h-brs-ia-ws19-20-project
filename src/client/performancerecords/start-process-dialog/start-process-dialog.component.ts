import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { AppState } from '../../app/store/reducers';
import { selectSalesmanById } from '../../app/store/app.selectors';
import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { startSingleCalculationProcess } from '../../app/store/actions/app.actions';
import { StartProcessPayload } from '../../app/model';

@Component({
    selector: 'app-start-process-dialog',
    templateUrl: './start-process-dialog.component.html',
    styleUrls: ['./start-process-dialog.component.scss']
})
export class StartProcessDialogComponent implements OnInit {

    salesman: Observable<string>;

    // Target Values
    leaderShip: FormControl;
    openness: FormControl;
    social: FormControl;
    attitude: FormControl;
    communication: FormControl;
    integrity: FormControl;
    targetValues: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public process: { employeeId: number, year: number },
        private store: Store<AppState>,
        private dialogRef: MatDialogRef<StartProcessDialogComponent>,
    ) {
    }

    ngOnInit() {
        this.salesman = this.store.select(selectSalesmanById(this.process.employeeId)).pipe(
            map(salesman => salesman.name),
        );
        this.leaderShip = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.openness = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.social = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.attitude = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.communication = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.integrity = new FormControl('', [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.targetValues = new FormGroup({
            leadership: this.leaderShip,
            openness: this.openness,
            social: this.social,
            attitude: this.attitude,
            communication: this.communication,
            integrity: this.integrity,
        });
    }

    start() {
        const payload: StartProcessPayload = {
            employeeId: this.process.employeeId,
            year: this.process.year,
            socialPerformance: [
                {
                    id: 1,
                    score: this.leaderShip.value,
                },
                {
                    id: 2,
                    score: this.openness.value,
                },
                {
                    id: 3,
                    score: this.social.value,
                },
                {
                    id: 4,
                    score: this.attitude.value,
                },
                {
                    id: 5,
                    score: this.communication.value,
                },
                {
                    id: 6,
                    score: this.integrity.value,
                },
            ]
        };
        this.store.dispatch(startSingleCalculationProcess({payload}));
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
