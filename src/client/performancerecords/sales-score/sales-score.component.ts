import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sales-score',
    templateUrl: './sales-score.component.html',
    styleUrls: ['./sales-score.component.scss']
})
export class SalesScoreComponent implements OnInit {

    @Input()
    companyName: string;

    @Input()
    amount: number;

    constructor() {
    }

    ngOnInit() {
    }

}
