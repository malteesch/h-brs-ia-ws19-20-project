import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { currentTheme } from '../themes/store/themes.selectors';
import { Observable } from 'rxjs';
import { ThemeState } from '../themes/store/reducers';
import { initializeApp } from './store/actions/app.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    theme$: Observable<string>;

    constructor(private store: Store<ThemeState>) {
    }

    ngOnInit(): void {
        this.store.dispatch(initializeApp());
        this.theme$ = this.store.select(currentTheme);
    }
}
