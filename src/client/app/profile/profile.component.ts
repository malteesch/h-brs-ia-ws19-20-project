import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Bonus, PerformanceRecord, ProcessStatus, SalesManProfile } from '../model';
import { map, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers';
import { selectSalesmanById } from '../store/app.selectors';
import { environment } from '../../environments/environment';
import { MatDialog } from '@angular/material';
import { BonusDetailsDialogComponent } from '../../performancerecords/bonus-details-dialog/bonus-details-dialog.component';
import { Title } from '@angular/platform-browser';
import { hasRole } from '../../auth/store/auth.selectors';
import { Role } from '../../auth/model';
import { approveProcessSalesman } from '../store/actions/app.actions';
import { ApproveBySalesmanDialogComponent } from '../../home/approve-by-salesman-dialog/approve-by-salesman-dialog.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

    salesman$: Observable<SalesManProfile>;
    performanceRecords$: Observable<Array<PerformanceRecord>>;
    name$: Observable<string>;
    jobTitle$: Observable<string>;
    pictureUrl: Observable<string>;
    isSalesman$ = new BehaviorSubject<boolean>(false);
    private salesmanSubscription$: Subscription = new Subscription();
    private isSalesmanSubscription = new Subscription();
    private hasApprovedSubscription = new Subscription();

    constructor(
        private route: ActivatedRoute,
        private store: Store<AppState>,
        private dialog: MatDialog,
        private title: Title
    ) {
        this.isSalesmanSubscription = this.store
            .select(hasRole, {requiredRoles: [Role.SALESMAN]})
            .subscribe(isSalesman => this.isSalesman$.next(isSalesman));
    }

    ngOnInit() {
        this.salesman$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => this.store.select(selectSalesmanById(parseInt(params.get('id'), 10)))),
        );

        this.salesmanSubscription$ = this.salesman$
            .subscribe(salesman => this.title.setTitle(`${salesman ? salesman.name : 'Profile'} | Performance Cockpit`));

        this.performanceRecords$ = this.salesman$.pipe(
            map(salesman => {
                if (salesman) {
                    return salesman.performanceRecords.slice().sort((a, b) => b.year - a.year);
                } else {
                    return [];
                }
            })
        );
        this.name$ = this.salesman$.pipe(
            map(salesman => {
                if (salesman) {
                    return salesman.name;
                }
            })
        );

        this.jobTitle$ = this.salesman$.pipe(
            map(salesman => {
                if (salesman) {
                    return salesman.jobTitle;
                }
            })
        );

        this.pictureUrl = this.salesman$.pipe(
            map(salesman => {
                if (salesman) {
                    return `${environment.backendBaseUrl}/${salesman.imgUrl}`;
                }
            })
        );
    }

    evalProcessStatus(processStatus: ProcessStatus): string {
        if (processStatus.finished) {
            return 'Approved';
        } else if (!processStatus.started) {
            return 'Not started';
        } else {
            return 'In Progress';
        }
    }

    evalBonus(bonus: Bonus): number {
        return bonus.calculatedBonus + bonus.extraBonus;
    }

    bonusClicked(event, performanceRecord: PerformanceRecord) {
        event.stopPropagation();
        this.dialog.open(BonusDetailsDialogComponent, {
            data: performanceRecord,
            width: '1400px',
            autoFocus: false
        });
    }

    ngOnDestroy(): void {
        this.salesmanSubscription$.unsubscribe();
        this.isSalesmanSubscription.unsubscribe();
        this.hasApprovedSubscription.unsubscribe();
    }

    approvableBySalesman(performanceRecord: PerformanceRecord) {
        return this.isSalesman$.value && performanceRecord.processStatus.approvedByCEO
            && performanceRecord.processStatus.approvedByHR
            && !performanceRecord.processStatus.approvedByEmployee;
    }

    approveBySalesman(event: MouseEvent, performanceRecord: PerformanceRecord) {
        event.stopPropagation();
        this.hasApprovedSubscription = this.dialog.open(ApproveBySalesmanDialogComponent, {
            autoFocus: false
        }).afterClosed().subscribe(hasAccepted => {
            if (hasAccepted) {
                const idString = performanceRecord.id.toString(10);
                this.store.dispatch(approveProcessSalesman({
                    employeeId: parseInt(idString.substr(0, idString.length - 4), 10),
                    performanceRecordId: performanceRecord.id
                }));
            }
        });
    }
}
