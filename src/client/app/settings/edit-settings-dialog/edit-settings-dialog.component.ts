import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Settings } from '../../model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/reducers';
import { updateSettings } from '../../store/actions/app.actions';
import { SettingsYearValidatorService } from '../settings-year-validator.service';

@Component({
    selector: 'app-edit-settings-dialog',
    templateUrl: './edit-settings-dialog.component.html',
    styleUrls: ['./edit-settings-dialog.component.scss']
})
export class EditSettingsDialogComponent implements OnInit {

    year: FormControl;

    // Customer Ratings
    aLevel: FormControl;
    bLevel: FormControl;
    cLevel: FormControl;
    customerRatingFactors: FormGroup;

    // Social performance factors
    maxValNoBonus: FormControl;
    fixValAlmostReached: FormControl;
    fixValReached: FormControl;
    factorReached: FormControl;
    socialPerformanceCalculationParams: FormGroup;

    // Target Values
    leaderShip: FormControl;
    openness: FormControl;
    social: FormControl;
    attitude: FormControl;
    communication: FormControl;
    integrity: FormControl;
    targetValues: FormGroup;

    editSettingsForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public settings: Settings,
        private dialogRef: MatDialogRef<EditSettingsDialogComponent>,
        private store: Store<AppState>,
        private yearValidatorService: SettingsYearValidatorService,
    ) {
    }


    ngOnInit(): void {
        this.year = new FormControl(this.settings.yearOfValidity, {
                updateOn: 'change',
                validators: [Validators.required],
                asyncValidators: [
                    this.yearValidatorService.validateYear(this.settings).bind(this.yearValidatorService)
                ]
            },
        );

        this.aLevel = new FormControl(this.settings.customerRatingFactors.firstLevelFactor, [Validators.required, Validators.min(1)]);
        this.bLevel = new FormControl(this.settings.customerRatingFactors.secondLevelFactor, [Validators.required, Validators.min(1)]);
        this.cLevel = new FormControl(this.settings.customerRatingFactors.thirdLevelFactor, [Validators.required, Validators.min(1)]);
        this.customerRatingFactors = new FormGroup({
            firstLevelFactor: this.aLevel,
            secondLevelFactor: this.bLevel,
            thirdLevelFactor: this.cLevel,
        });

        this.maxValNoBonus = new FormControl(this.settings.socialPerformanceCalculationParams.maxValNoBonus, Validators.required);
        // tslint:disable-next-line:max-line-length
        this.fixValAlmostReached = new FormControl(this.settings.socialPerformanceCalculationParams.fixValAlmostReached, Validators.required);
        this.fixValReached = new FormControl(this.settings.socialPerformanceCalculationParams.fixValReached, Validators.required);
        this.factorReached = new FormControl(this.settings.socialPerformanceCalculationParams.factorReached, Validators.required);
        this.socialPerformanceCalculationParams = new FormGroup({
            maxValNoBonus: this.maxValNoBonus,
            fixValAlmostReached: this.fixValAlmostReached,
            fixValReached: this.fixValReached,
            factorReached: this.factorReached
        });

        this.leaderShip = new FormControl(this.settings.socialPerformanceTargetValues[0].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.openness = new FormControl(this.settings.socialPerformanceTargetValues[1].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.social = new FormControl(this.settings.socialPerformanceTargetValues[2].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.attitude = new FormControl(this.settings.socialPerformanceTargetValues[3].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.communication = new FormControl(this.settings.socialPerformanceTargetValues[4].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.integrity = new FormControl(this.settings.socialPerformanceTargetValues[5].targetValue, [
            Validators.required,
            Validators.min(0),
            Validators.max(10)
        ]);
        this.targetValues = new FormGroup({
            leadership: this.leaderShip,
            openness: this.openness,
            social: this.social,
            attitude: this.attitude,
            communication: this.communication,
            integrity: this.integrity,
        });

        this.editSettingsForm = new FormGroup({
            yearOfValidity: this.year,
            customerRatingFactors: this.customerRatingFactors,
            socialPerformanceCalculationParams: this.socialPerformanceCalculationParams,
            socialPerformanceTargetValues: this.targetValues
        });
    }

    saveSettings() {
        const newSettings: Settings = {
            yearOfValidity: this.editSettingsForm.value.yearOfValidity,
            socialPerformanceCalculationParams: this.editSettingsForm.value.socialPerformanceCalculationParams,
            customerRatingFactors: this.editSettingsForm.value.customerRatingFactors,
            socialPerformanceTargetValues: [
                {
                    id: 1,
                    category: 'Leadership Competence',
                    targetValue: this.targetValues.value.leadership
                },
                {
                    id: 2,
                    category: 'Openness to Employee',
                    targetValue: this.targetValues.value.openness
                },
                {
                    id: 3,
                    category: 'Social Behaviour to Employee',
                    targetValue: this.targetValues.value.social
                },
                {
                    id: 4,
                    category: 'Attitude toward Client',
                    targetValue: this.targetValues.value.attitude
                },
                {
                    id: 5,
                    category: 'Communication Skills',
                    targetValue: this.targetValues.value.communication
                },
                {
                    id: 6,
                    category: 'Integrity to Company',
                    targetValue: this.targetValues.value.integrity
                },
            ],
            default: false,
        };
        this.store.dispatch(updateSettings({newSettings}));
    }
}
