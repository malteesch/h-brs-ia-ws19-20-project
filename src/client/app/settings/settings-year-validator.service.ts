import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers';
import { selectSettings } from '../store/app.selectors';
import { map, take } from 'rxjs/operators';
import { Settings } from '../model';

@Injectable({
    providedIn: 'root'
})
export class SettingsYearValidatorService implements AsyncValidator {

    passedSetting: Settings;

    constructor(private store: Store<AppState>) {
    }

    validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
        return this.store.select(selectSettings).pipe(
            take(1),
            map((settings) => {
                    return settings
                        // tslint:disable-next-line:max-line-length
                        .filter(setting => setting.yearOfValidity === control.value && !setting.default && this.passedSetting.default).length > 0
                        ? {yearTaken: 'The selected year already has settings'}
                        : null;
                }
            ),
        );
    }

    validateYear(setting: Settings) {
        this.passedSetting = setting;
        return this.validate;
    }
}
