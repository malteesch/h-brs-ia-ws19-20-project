import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppState } from '../store/reducers';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Settings } from '../model';
import { selectSettings } from '../store/app.selectors';
import { MatDialog } from '@angular/material/dialog';
import { EditSettingsDialogComponent } from './edit-settings-dialog/edit-settings-dialog.component';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    defaultSettings: Settings = {
        yearOfValidity: new Date().getFullYear(),
        socialPerformanceTargetValues: [
            {
                category: 'Leadership Competence',
                targetValue: 4,
                id: 1
            },
            {
                category: 'Openness to Employee',
                targetValue: 4,
                id: 2
            },
            {
                category: 'Social Behaviour to Employee',
                targetValue: 4,
                id: 3
            },
            {
                category: 'Attitude toward Client',
                targetValue: 4,
                id: 4
            },
            {
                category: 'Communication Skills',
                targetValue: 4,
                id: 5
            },
            {
                category: 'Integrity to Company',
                targetValue: 4,
                id: 6
            }
        ],
        socialPerformanceCalculationParams: {
            maxValNoBonus: -2,
            fixValAlmostReached: 20,
            fixValReached: 50,
            factorReached: 50
        },
        customerRatingFactors: {
            firstLevelFactor: 5,
            secondLevelFactor: 3,
            thirdLevelFactor: 1,
        },
        default: true
    };

    settings$: Observable<Array<Settings>>;

    constructor(private title: Title, private store: Store<AppState>, private dialog: MatDialog) {
    }

    ngOnInit() {
        this.title.setTitle('Settings | Performance Cockpit');
        this.settings$ = this.store.select(selectSettings);
    }

    openEditSettings(setting: Settings) {
        this.dialog.open(EditSettingsDialogComponent, {
            data: setting,
        });
    }
}
