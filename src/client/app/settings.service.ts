import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Settings } from './model';
import { environment } from '../environments/environment';
import { Store } from '@ngrx/store';
import { hasRole } from '../auth/store/auth.selectors';
import { AuthState } from '../auth/store/reducers';
import { Role } from '../auth/model';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    hasSettingsPermission$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(private http: HttpClient, private store: Store<AuthState>) {
        this.store
            .select(hasRole, {requiredRoles: [Role.CEO, Role.ADMIN, Role.HR]})
            .subscribe((hasPermission) => this.hasSettingsPermission$.next(hasPermission));
    }

    getAllSettings(): Observable<Array<Settings>> {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });
        if (this.hasSettingsPermission$.value) {
            return this.http.get<Array<Settings>>(`${environment.backendBaseUrl}/api/settings`, {headers});
        }
        return of([]);
    }

    updateSettings(settings: Settings): Observable<any> {
        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });
        return this.http.put<Array<Settings>>(`${environment.backendBaseUrl}/api/settings/${settings.yearOfValidity}`,
            settings, {headers});
    }
}
