import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import {
    approveProcessCEO,
    approveProcessFailedCEO,
    approveProcessFailedHR,
    approveProcessFailedSalesman,
    approveProcessHR,
    approveProcessSalesman,
    approveProcessSuccessfulCEO,
    approveProcessSuccessfulHR,
    approveProcessSuccessfulSalesman,
    giveFeedback,
    giveFeedbackFailed,
    giveFeedbackSuccessful,
    initializeApp,
    loadSalesmen,
    loadSalesmenFailed,
    loadSalesmenSuccessful,
    loadSettings,
    loadSettingsFailed,
    loadSettingsSuccessful,
    startSingleCalculationProcess,
    startSingleCalculationProcessFailed,
    startSingleCalculationProcessSuccessful,
    updateSettings,
    updateSettingsFailed,
    updateSettingsSuccessful
} from './actions/app.actions';
import { catchError, exhaustMap, map, mergeAll, mergeMap, tap } from 'rxjs/operators';
import { SalesmenService } from '../salesmen.service';
import { PerformancerecordsService } from '../../performancerecords/performancerecords.service';
import { AppState } from './reducers';
import { StartProcessPayload } from '../model';
import { loadToken } from '../../auth/store/auth.actions';
import { loadTheme } from '../../themes/store/themes.actions';
import { SettingsService } from '../settings.service';
import { NotificationService } from '../../notification/notification.service';

@Injectable()
export class AppEffects {

    initializeApp$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(initializeApp),
        map(() => [loadToken(), loadTheme()]),
        mergeAll()
    ));
    successfulUpdates$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(
            startSingleCalculationProcessSuccessful,
            giveFeedbackSuccessful,
            approveProcessSuccessfulCEO,
            approveProcessSuccessfulHR,
            approveProcessSuccessfulSalesman
        ),
        map(() => loadSalesmen())
    ));

    startSingleProcess$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(startSingleCalculationProcess),
        exhaustMap((payload: { payload: StartProcessPayload }) => this.performanceRecordsService
            .startProcess(payload.payload)
            .pipe(
                map(() => startSingleCalculationProcessSuccessful()),
                catchError(err => {
                    console.log(err);
                    return of(startSingleCalculationProcessFailed());
                }))
        )),
    );

    startSingleProcessSuccess$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(startSingleCalculationProcessSuccessful),
        tap(() => this.notification.done('Process started'))
    ), {dispatch: false});

    startSingleProcessFailed$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(startSingleCalculationProcessFailed),
        tap(() => this.notification.error('Process not started'))
    ), {dispatch: false});

    loadSalesmen$: Observable<any> = createEffect(() => this.actions$.pipe(
        ofType(loadSalesmen),
        exhaustMap(() => this.salesmenService.loadSalesmen().pipe(
            map(salesmen => loadSalesmenSuccessful({salesmen})),
            catchError(err => of(loadSalesmenFailed())),
        )),
    ));

    loadSettings$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(loadSettings),
        exhaustMap(() => this.settingsService.getAllSettings().pipe(
            map(settings => loadSettingsSuccessful({settings})),
            catchError(() => of(loadSettingsFailed()))
        ))
    ));

    approveProcessCEO$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessCEO),
        exhaustMap(payload => this.performanceRecordsService.approveProcessCEO(payload).pipe(
            map(() => approveProcessSuccessfulCEO()),
            catchError(err => of(approveProcessFailedCEO()))
        ))
    ));

    approveProcessHR$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessHR),
        exhaustMap(payload => this.performanceRecordsService.approveProcessHR(payload).pipe(
            map(() => approveProcessSuccessfulHR()),
            catchError(err => of(approveProcessFailedHR()))
        ))
    ));

    approveProcessSalesman$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessSalesman),
        exhaustMap(payload => this.performanceRecordsService.approveProcessSalesman(payload).pipe(
            map(() => approveProcessSuccessfulSalesman()),
            catchError(err => of(approveProcessFailedSalesman()))
        ))
    ));

    updateSettings$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(updateSettings),
        exhaustMap((props) => this.settingsService.updateSettings(props.newSettings).pipe(
            map(() => updateSettingsSuccessful()),
            catchError(err => of(updateSettingsFailed())),
        )),
    ));

    updateSettingsSuccessful$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(updateSettingsSuccessful),
        mergeMap(() => of([loadSettings(), loadSalesmen()])),
        mergeAll(),
        tap(() => this.notification.done('Settings updated')),
    ));

    updateSettingsFailed$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(updateSettingsFailed),
        tap(() => this.notification.error('Settings not updated')),
    ), {dispatch: false});

    approveProcessSuccessfullCEO$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessSuccessfulCEO),
        tap(() => this.notification.done('Process approved')),
    ), {dispatch: false});

    approveProcessFailedCEO$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessFailedCEO),
        tap(() => this.notification.error('Process not approved')),
    ), {dispatch: false});

    approveProcessSuccessfullHR$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessSuccessfulHR),
        tap(() => this.notification.done('Process approved')),
    ), {dispatch: false});

    approveProcessFailedHR$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessFailedHR),
        tap(() => this.notification.error('Process not approved')),
    ), {dispatch: false});

    approveProcessSuccessfullSalesman$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessSuccessfulSalesman),
        tap(() => this.notification.done('Process accepted and finished')),
    ), {dispatch: false});

    approveProcessFailedSalesman$: Observable<Action> = createEffect(() => this.actions$.pipe(
        ofType(approveProcessFailedSalesman),
        tap(() => this.notification.error('Process was not accepted')),
    ), {dispatch: false});

    constructor(
        private actions$: Actions,
        private store: Store<AppState>,
        private salesmenService: SalesmenService,
        private performanceRecordsService: PerformancerecordsService,
        private settingsService: SettingsService,
        private notification: NotificationService,
    ) {
    }
}
