import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { Order, PerformanceRecord, ProcessStatus, SalesManProfile, Settings } from '../model';
import { AppState } from './reducers';
import { Label } from 'ng2-charts';
import { ChartDataSets } from 'chart.js';

export const selectSalesmen: MemoizedSelector<AppState, Array<SalesManProfile>> = createFeatureSelector('salesmen');

export const selectSalesmanById = (id: number) => createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => salesmen.find(salesman => salesman.employeeId === id)
);

export const selectPerformanceRecord = (employeeId: number, recordId: number) => createSelector(
    selectSalesmanById(employeeId),
    (salesman: SalesManProfile) => salesman.performanceRecords.find(record => record.id === recordId)
);

export const selectSettings = createFeatureSelector<Array<Settings>>('settings');

export const selectUnstartedProcesses = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>): Array<{ employeeId: number, year: number }> => {
        const openProcesses: Array<{ employeeId: number, year: number }> = [];
        salesmen.forEach(salesman => {
            const years = salesman.performanceRecords.map(record => record.year);
            const yearsWithoutProcess = salesman.orders
                .map(order => new Date(order.date).getFullYear())
                .filter((year) => {
                    return !years.includes(year);
                });
            yearsWithoutProcess
                .filter((v, i) => yearsWithoutProcess.indexOf(v) === i)
                .forEach(year => {
                    const newItem = {employeeId: salesman.employeeId, year};
                    if (!openProcesses.includes(newItem)) {
                        openProcesses.push(newItem);
                    }
                });
        });
        return openProcesses;
    }
);

export const selectUnapprovedByCEO = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => {
        const unApproved = [];
        salesmen
            .forEach(salesman => salesman.performanceRecords
                .filter(record => !record.processStatus.approvedByCEO)
                .forEach(record => unApproved.push(record)));
        return unApproved;
    }
);

export const selectUnapprovedByHR = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => {
        const unApproved = [];
        salesmen
            .forEach(salesman => salesman.performanceRecords
                .filter(record => record.processStatus.approvedByCEO && !record.processStatus.approvedByHR)
                .forEach(record => unApproved.push(record)));
        return unApproved;
    }
);

export const selectAverageSocialScores: MemoizedSelector<AppState, { labels: Label[], sets: ChartDataSets[] }> = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => {
        let labels = [];
        if (salesmen[0] && salesmen[0].performanceRecords[0]) {
            labels = salesmen[0].performanceRecords[0].socialScores.map(score => score.category);
        }
        const sets: ChartDataSets[] = [];
        salesmen.forEach(salesman => {
            salesman.performanceRecords.forEach(record => {
                sets.push({data: record.socialScores.map(score => score.actualValue), label: record.year.toString(10)});
            });
        });
        return {labels, sets};
    }
);

export const selectAllOrders = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => {
        const orders: Array<Order> = [];
        salesmen.forEach(salesman => orders.push(...salesman.orders));
        return orders;
    }
);

export const selectAllPerformanceRecords: MemoizedSelector<AppState, Array<PerformanceRecord>> = createSelector(
    selectSalesmen,
    (salesmen: Array<SalesManProfile>) => {
        const records: Array<PerformanceRecord> = [];
        salesmen.forEach(salesman => records.push(...salesman.performanceRecords));
        return records;
    }
);
