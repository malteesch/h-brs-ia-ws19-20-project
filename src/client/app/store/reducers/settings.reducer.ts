import { Action, createReducer, on } from '@ngrx/store';
import { loadSettingsSuccessful } from '../actions/app.actions';
import { Settings } from '../../model';
import { logoutSuccessful } from '../../../auth/store/auth.actions';

const settingsReducer = createReducer(
    [],
    on(loadSettingsSuccessful, (state: Array<Settings>, payload: { settings: Array<Settings> }) => payload.settings),
    on(logoutSuccessful, () => []),
);

export function reducer(state: Array<Settings> | undefined, action: Action) {
    return settingsReducer(state, action);
}
