import { Action, createReducer, on } from '@ngrx/store';
import { SalesManProfile } from '../../model';
import { loadSalesmenSuccessful } from '../actions/app.actions';
import { logoutSuccessful } from '../../../auth/store/auth.actions';

const salesmenReducer = createReducer(
    [],
    on(loadSalesmenSuccessful, (state, payload: { salesmen: Array<SalesManProfile> }) => payload.salesmen),
    on(logoutSuccessful, () => [])
);

export function reducer(state: Array<SalesManProfile> | undefined, action: Action) {
    return salesmenReducer(state, action);
}
