import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { SalesManProfile, Settings } from '../../model';
import * as fromSalesmen from './salesmen.reducer';
import * as fromSettings from './settings.reducer';

export interface AppState {
    salesmen: Array<SalesManProfile>;
    settings: Array<Settings>;
}

export const reducers: ActionReducerMap<AppState> = {
    salesmen: fromSalesmen.reducer,
    settings: fromSettings.reducer,
};

export const metaReducers: Array<MetaReducer<AppState>> = !environment.production ? [] : [];
