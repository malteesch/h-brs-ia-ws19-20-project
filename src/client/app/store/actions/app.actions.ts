import { createAction, props } from '@ngrx/store';
import { Feedback, SalesManProfile, Settings, StartProcessPayload } from '../../model';
import { MatDialogRef } from '@angular/material/dialog';
import { EditSettingsDialogComponent } from '../../settings/edit-settings-dialog/edit-settings-dialog.component';

export const initializeApp = createAction(
    '[APP] Initialize app'
);

export const loadSalesmen = createAction(
    '[APP] Load salesmen'
);

export const loadSalesmenSuccessful = createAction(
    '[APP] Load salesmen successful',
    props<{ salesmen: Array<SalesManProfile> }>()
);

export const loadSalesmenFailed = createAction(
    '[APP] Load salesmen failed'
);

export const startSingleCalculationProcess = createAction(
    '[PROCESS] Start single calculation process',
    props<{ payload: StartProcessPayload }>()
);

export const startSingleCalculationProcessSuccessful = createAction(
    '[PROCESS] Single process start successful'
);

export const startSingleCalculationProcessFailed = createAction(
    '[PROCESS] Single process start failed'
);

export const startAllCalculationProcesses = createAction(
    '[PROCESS] Start all calculation processes',
    props<{ openProcesses: Array<{ employeeId: number, year: number }> }>()
);

export const startAllCalculationProcessesSuccessful = createAction(
    '[PROCESS] All processes start successful'
);

export const startAllCalculationProcessesFailed = createAction(
    '[PROCESS] All processes start failed'
);

export const giveFeedback = createAction(
    '[PROCESS] Give feedback',
    props<{ employeeId: number, performanceRecordId: number, feedback: Feedback }>()
);

export const giveFeedbackSuccessful = createAction(
    '[PROCESS] Give feedback successful'
);

export const giveFeedbackFailed = createAction(
    '[PROCESS] Give feedback failed'
);

export const approveProcessCEO = createAction(
    '[PROCESS] Approve process CEO',
    props<{ employeeId: number, performanceRecordId: number, extra: number, feedback: Feedback }>()
);

export const approveProcessHR = createAction(
    '[PROCESS] Approve process HR',
    props<{ employeeId: number, performanceRecordId: number }>()
);

export const approveProcessSalesman = createAction(
    '[PROCESS] Approve process Salesman',
    props<{ employeeId: number, performanceRecordId: number }>()
);

export const approveProcessSuccessfulCEO = createAction(
    '[PROCESS] Approve process successful CEO'
);

export const approveProcessSuccessfulHR = createAction(
    '[PROCESS] Approve process successful HR'
);

export const approveProcessSuccessfulSalesman = createAction(
    '[PROCESS] Approve process successful Salesman'
);

export const approveProcessFailedCEO = createAction(
    '[PROCESS] Approve process failed CEO'
);

export const approveProcessFailedHR = createAction(
    '[PROCESS] Approve process failed HR'
);

export const approveProcessFailedSalesman = createAction(
    '[PROCESS] Approve process failed Salesman'
);

export const loadSettings = createAction(
    '[SETTINGS] Load settings'
);

export const loadSettingsSuccessful = createAction(
    '[SETTINGS] Load settings successful',
    props<{ settings: Array<Settings> }>()
);

export const loadSettingsFailed = createAction(
    '[SETTINGS] Load settings failed'
);

export const updateSettings = createAction(
    '[SETTINGS] Update settings',
    props<{ newSettings: Settings }>()
);

export const updateSettingsSuccessful = createAction(
    '[SETTINGS] Update settings successful',
);

export const updateSettingsFailed = createAction(
    '[SETTINGS] Update settings failed',
);

