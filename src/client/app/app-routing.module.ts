import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from '../shell/shell/shell.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { SalesmanResolver } from './salesman.resolver';
import { OnlySalesmanGuard } from '../auth/guards/only-salesman.guard';
import { SettingsComponent } from './settings/settings.component';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../auth/model';

const routes: Routes = [
    {
        path: '', component: ShellComponent,
        children: [
            {path: 'home', component: HomeComponent, canActivate: [OnlySalesmanGuard]},
            {
                path: 'settings',
                component: SettingsComponent,
                canActivate: [OnlySalesmanGuard, RoleGuard],
                data: {requiredRoles: [Role.CEO, Role.ADMIN, Role.HR]}
            },
            {path: 'profile/:id', component: ProfileComponent, resolve: {salesman: SalesmanResolver}},
            {path: '', redirectTo: 'home', pathMatch: 'full'},
        ],
        canActivateChild: [AuthGuard],
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {enableTracing: false})],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
