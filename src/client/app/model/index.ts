export interface SalesManProfile {
    employeeId: number;
    hrmId: number;
    name: string;
    performanceRecords: Array<PerformanceRecord>;
    orders: Array<Order>;
    jobTitle: string;
    imgUrl: string;
}

export interface SocialScore {
    id: number;
    category: string;
    targetValue: number;
    actualValue: number;
    bonus: number;
    approved: boolean;
}

export interface SalesScore {
    product: Product;
    customer: Customer;
    itemsSold: number;
    bonus: number;
    approved: boolean;
}

export interface PerformanceRecord {
    id: number;
    socialScores: Array<SocialScore>;
    salesScores: Array<SalesScore>;
    year: number;
    processStatus: ProcessStatus;
    bonus: Bonus;
    feedback: Feedback;
}

export interface Feedback {
    text: string;
    fromUser: {
        fullName: string;
        jobTitle: string;
    };
}

export interface Bonus {
    calculatedBonus: number;
    extraBonus: number;
}

export interface Order {
    customer: Customer;
    total: number;
    positions: Array<OrderPosition>;
    date: Date;
}

export interface Customer {
    id: string;
    name: string;
    rating: number;
}

export interface OrderPosition {
    product: Product;
    amount: number;
}

export interface Product {
    id: string;
    name: string;
}

export interface ProcessStatus {
    finished: boolean;
    feedbackGiven: boolean;
    started: boolean;
    approvedByCEO: boolean;
    approvedByHR: boolean;
    approvedByEmployee: boolean;
}

export interface Settings {
    yearOfValidity: number;
    socialPerformanceTargetValues: Array<SocialPerformanceTargetValue>;
    socialPerformanceCalculationParams: SocialPerformanceCalculationParams;
    customerRatingFactors: CustomerRatingFactors;
    default: boolean;
}

export interface SocialPerformanceTargetValue {
    id: number;
    category: string;
    targetValue: number;
}

export interface SocialPerformanceCalculationParams {
    maxValNoBonus: number;
    fixValAlmostReached: number;
    fixValReached: number;
    factorReached: number;
}

export interface CustomerRatingFactors {
    firstLevelFactor: number;
    secondLevelFactor: number;
    thirdLevelFactor: number;
}

export interface StartProcessPayload {
    employeeId: number;
    year: number;
    socialPerformance: Array<{ id: number, score: number }>;
}
