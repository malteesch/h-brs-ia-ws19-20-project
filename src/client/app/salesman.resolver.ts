import { Observable } from 'rxjs';
import { SalesManProfile } from './model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './store/reducers';
import { selectSalesmanById } from './store/app.selectors';
import { take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SalesmanResolver implements Resolve<SalesManProfile> {

    constructor(private store: Store<AppState>) {
    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<SalesManProfile> | Promise<SalesManProfile> | SalesManProfile {
        return this.store.select(selectSalesmanById(parseInt(route.params.id, 10))).pipe(
            take(1),
        );
    }
}
