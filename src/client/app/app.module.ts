import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './store/reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '../auth/auth.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HomeComponent } from './home/home.component';
import { ShellModule } from '../shell/shell.module';
import { ThemesModule } from '../themes/themes.module';
import { PerformancerecordsModule } from '../performancerecords/performancerecords.module';
import { ProfileComponent } from './profile/profile.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { AppEffects } from './store/app.effects';
import { NotificationModule } from '../notification/notification.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HomeModule } from '../home/home.module';
import { SettingsComponent } from './settings/settings.component';
import { MatCheckboxModule, MatTabsModule } from '@angular/material';
import { EditSettingsDialogComponent } from './settings/edit-settings-dialog/edit-settings-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ProfileComponent,
        SettingsComponent,
        EditSettingsDialogComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCardModule,
        HttpClientModule,
        AuthModule,
        ShellModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
            },
        }),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
        EffectsModule.forRoot([AppEffects]),
        FormsModule,
        ReactiveFormsModule,
        MatToolbarModule,
        ThemesModule,
        PerformancerecordsModule,
        MatExpansionModule,
        MatIconModule,
        MatChipsModule,
        NotificationModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        HomeModule,
        NotificationModule,
        MatTabsModule,
        MatCheckboxModule,
        MatDialogModule,
    ],
    providers: [],
    entryComponents: [EditSettingsDialogComponent],
    bootstrap: [AppComponent],
})
export class AppModule {
}
