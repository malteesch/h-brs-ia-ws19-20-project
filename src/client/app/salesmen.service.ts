import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SalesManProfile } from './model';
import { Store } from '@ngrx/store';
import { AuthState } from '../auth/store/reducers';
import { selectAuth } from '../auth/store/auth.selectors';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SalesmenService {

    constructor(private http: HttpClient, private store: Store<AuthState>) {
    }

    loadSalesmen(): Observable<Array<SalesManProfile>> {

        function getCorrectEndpoint(store: Store<AuthState>): string {
            let url = '';
            const sub = store.select(selectAuth).subscribe((state: AuthState) => {
                if (state.roles.length === 1 && state.roles[0] === 'SALESMAN') {
                    url = `${environment.backendBaseUrl}/api/salesmen/${state.id}`;
                } else {
                    url = `${environment.backendBaseUrl}/api/salesmen`;
                }
            });
            sub.unsubscribe();
            return url;
        }

        const headers: HttpHeaders = new HttpHeaders({
            ['Accept']: 'application/json',
        });

        return this.http.get<Array<SalesManProfile>>(getCorrectEndpoint(this.store), {
            headers,
        }).pipe(
            map(res => {
                if (Array.isArray(res)) {
                    return res;
                } else {
                    return [res];
                }
            })
        );
    }
}
