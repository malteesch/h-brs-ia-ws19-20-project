import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers';
import { Observable } from 'rxjs';
import { Role } from '../../auth/model';
import { hasRole } from '../../auth/store/auth.selectors';
import { selectUnapprovedByCEO, selectUnapprovedByHR } from '../store/app.selectors';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    Role = Role;

    unapprovedByCEO$ = this.store.select(selectUnapprovedByCEO);
    unapprovedByHR$ = this.store.select(selectUnapprovedByHR);

    constructor(
        private title: Title,
        private store: Store<AppState>,
    ) {
    }

    ngOnInit() {
        this.title.setTitle('Home | Performance Cockpit');
    }

    hasRole(...roles: Array<Role>): Observable<boolean> {
        return this.store.select(hasRole, {requiredRoles: roles});
    }
}
